package in.waycool.outgrow.Models;

import java.util.List;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class CropTypesModel {


    private int status;

    private String message;

    private List<Crops> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Crops> getData() {
        return data;
    }

    public void setData(List<Crops> data) {
        this.data = data;
    }


    public class Crops
    {
        private String crop_id;
        private String crop_name;

        public String getCrop_id() {
            return crop_id;
        }

        public void setCrop_id(String crop_id) {
            this.crop_id = crop_id;
        }

        public String getCrop_name() {
            return crop_name;
        }

        public void setCrop_name(String crop_name) {
            this.crop_name = crop_name;
        }
    }

}

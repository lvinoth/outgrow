package in.waycool.outgrow.Models;

import java.util.List;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class CropsModel {


    private int status;

    private String message;

    private List<CropData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<CropData> getData() {
        return data;
    }

    public void setData(List<CropData> data) {
        this.data = data;
    }


    public class CropData
    {
        private String crop_details_id;
        private String crop_name;
        private String start_date;
        private String end_date;
        private String total_cost;
        private String total_sale_amount;

        public String getCrop_details_id() {
            return crop_details_id;
        }

        public void setCrop_details_id(String crop_details_id) {
            this.crop_details_id = crop_details_id;
        }

        public String getCrop_name() {
            return crop_name;
        }

        public void setCrop_name(String crop_name) {
            this.crop_name = crop_name;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getEnd_date() {
            return end_date;
        }

        public void setEnd_date(String end_date) {
            this.end_date = end_date;
        }

        public String getTotal_cost() {
            return total_cost;
        }

        public void setTotal_cost(String total_cost) {
            this.total_cost = total_cost;
        }

        public String getTotal_sale_amount() {
            return total_sale_amount;
        }

        public void setTotal_sale_amount(String total_sale_amount) {
            this.total_sale_amount = total_sale_amount;
        }
    }

}

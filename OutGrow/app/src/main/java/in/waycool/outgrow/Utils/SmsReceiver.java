package in.waycool.outgrow.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class SmsReceiver extends BroadcastReceiver {
    private static SmsListener mListener;
    Boolean isFromSunnyB;
    String abcd,xyz;
    private final String TAG = SmsReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data  = intent.getExtras();
        Object[] pdus = (Object[]) data.get("pdus");

        for(int i=0;i<pdus.length;i++){

            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

            String sender = smsMessage.getDisplayOriginatingAddress();
            isFromSunnyB =sender.endsWith("SUNNYB");  //Just to fetch otp sent from WNRCRP

            String messageBody = smsMessage.getMessageBody();
            Log.d(TAG, "SMS Body: " + messageBody);
//            abcd=messageBody.replaceAll("[^0-9]","");

            if(isFromSunnyB ) {
                mListener.messageReceived(abcd);
            }
        }
    }
    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }
}

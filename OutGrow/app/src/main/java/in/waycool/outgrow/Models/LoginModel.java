package in.waycool.outgrow.Models;

import java.util.List;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class LoginModel {


    private int status;

    private String message;

    private List<ExecutiveData> data;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ExecutiveData> getData() {
        return data;
    }

    public void setData(List<ExecutiveData> data) {
        this.data = data;
    }


    public class ExecutiveData
    {
        private String field_executive_id;
        private String field_executive_name;
        private String field_executive_mobile;
        private String field_executive_latitude;
        private String field_executive_longitude;
        private String field_executive_image;
        private String mobile_verified;

        public String getField_executive_id() {
            return field_executive_id;
        }

        public void setField_executive_id(String field_executive_id) {
            this.field_executive_id = field_executive_id;
        }

        public String getField_executive_name() {
            return field_executive_name;
        }

        public void setField_executive_name(String field_executive_name) {
            this.field_executive_name = field_executive_name;
        }

        public String getField_executive_mobile() {
            return field_executive_mobile;
        }

        public void setField_executive_mobile(String field_executive_mobile) {
            this.field_executive_mobile = field_executive_mobile;
        }

        public String getField_executive_latitude() {
            return field_executive_latitude;
        }

        public void setField_executive_latitude(String field_executive_latitude) {
            this.field_executive_latitude = field_executive_latitude;
        }

        public String getField_executive_longitude() {
            return field_executive_longitude;
        }

        public void setField_executive_longitude(String field_executive_longitude) {
            this.field_executive_longitude = field_executive_longitude;
        }

        public String getMobile_verified() {
            return mobile_verified;
        }

        public void setMobile_verified(String mobile_verified) {
            this.mobile_verified = mobile_verified;
        }

        public String getField_executive_image() {
            return field_executive_image;
        }

        public void setField_executive_image(String field_executive_image) {
            this.field_executive_image = field_executive_image;
        }
    }
}

package in.waycool.outgrow;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CampaignActivity extends AppCompatActivity {

    private ImageView backBtn;
    private ImageView campaignView;
    private TextView campaignText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign);
        campaignText = findViewById(R.id.campaignText);
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        campaignView = findViewById(R.id.campaignImage);
        boolean isCampaign = getIntent().getBooleanExtra("is_campaign",false);
        Drawable drawable;
        String txt;
        if (isCampaign)
        {
            drawable = getDrawable(R.drawable.my_soil_campaign_info);
            txt = "MySoil Campaign Info";
        }else
        {
            drawable = getDrawable(R.drawable.soil_sampling_help);
            txt = "Soil sampling help";
        }
        campaignView.setImageDrawable(drawable);
        campaignText.setText(txt);
    }
}

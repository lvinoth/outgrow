package in.waycool.outgrow.Utils;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class GPSTracker extends Service implements LocationListener {

    private final Context mContext;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    double altitude;
    double bearing;
    double speed;
    String dateTime;
    String deivceDateTime;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 1 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 5; // 5 Secs

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public GPSTracker(Context context) {
        this.mContext = context;
        getLocation();
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isGPSEnabled) {

                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("GPS Enabled", "GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            altitude = location.getAltitude();
                            bearing = location.getBearing();
                            speed = location.getSpeed();
                            dateTime = getFormattedDateTime(location.getTime());
                            deivceDateTime = getDeviceDateTime();
                            Log.d(GPSTracker.class.getSimpleName(), "From GPS");
                        }
                        else {
                            if (isNetworkEnabled) {
                                locationManager.requestLocationUpdates(
                                        LocationManager.NETWORK_PROVIDER,
                                        MIN_TIME_BW_UPDATES,
                                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                                Log.d("Network", "Network");
                                if (locationManager != null) {
                                    location = locationManager
                                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                    if (location != null) {
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                        altitude = location.getAltitude();
                                        bearing = location.getBearing();
                                        speed = location.getSpeed();
                                        dateTime = getFormattedDateTime(location.getTime());
                                        deivceDateTime = getDeviceDateTime();
                                        Log.d(GPSTracker.class.getSimpleName(), "From Network");
                                    }
                                }
                            }
                        }
                    }
                } else if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            altitude = location.getAltitude();
                            bearing = location.getBearing();
                            speed = location.getSpeed();
                            dateTime = getFormattedDateTime(location.getTime());
                            deivceDateTime = getDeviceDateTime();
                            Log.d(GPSTracker.class.getSimpleName(), "From Network");
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /**
     * Function to get latitude
     */
    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to get altitude
     */
    public double getAltitude() {
        if (location != null) {
            altitude = location.getAltitude();
        }

        // return altitude
        return altitude;
    }

    /**
     * Function to get speed
     */
    public double getSpeed() {
        if (location != null) {
            if (location.hasSpeed()) {
                speed = location.getSpeed();
            } else {
                speed = -1;
            }
            Log.d(GPSTracker.class.getSimpleName(), "From: " + location.getProvider());
        }
        return speed;
    }

    /**
     * Function to get bearing
     */
    public double getBearing() {
        if (location != null) {
            if (location.hasBearing()) {
                bearing = location.getBearing();
            } else {
                bearing = -1;
            }
            Log.d(GPSTracker.class.getSimpleName(), "From: " + location.getProvider());
        }

        return bearing;
    }


    public String getDateTime() {
        if (location != null) {
            long timeInMillis = location.getTime();
            dateTime = getFormattedDateTime(timeInMillis);
            Log.d(GPSTracker.class.getSimpleName(), "get Date: location not null");
        }
        return dateTime;
    }

    public String getDeviceDateTime() {
        Calendar calendar = Calendar.getInstance();
        long timeInMillis = calendar.getTimeInMillis();
        deivceDateTime = getFormattedDateTime(timeInMillis);
        Log.d(GPSTracker.class.getSimpleName(), "Device Date: " + deivceDateTime);
        return deivceDateTime;
    }

    public String getFormattedDateTime(long milliSecs) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSecs);
        String mDateTime = DateFormat.format("yyyy-MM-dd, HH:mm:ss", calendar).toString();
        Log.d(GPSTracker.class.getSimpleName(), "Date Time: " + dateTime);

        return mDateTime;
    }


    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public boolean isGPSTurnedOn()
    {
        this.getLocation();
        return this.isGPSEnabled;
    }
    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("");

        // Setting Dialog Message
        alertDialog.setMessage("Your GPS seems to be disabled, Please enable it.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}
package in.waycool.outgrow;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.text.TextUtils;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.waycool.outgrow.Models.RegistrationModel;
import in.waycool.outgrow.Retrofit.ApiClient;
import in.waycool.outgrow.Retrofit.ApiInterface;
import in.waycool.outgrow.Utils.GPSTracker;
import in.waycool.outgrow.Utils.SharedPreferenceUtility;
import in.waycool.outgrow.Utils.UtilsClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.widget.Toast.LENGTH_LONG;

public class RegistrationActivity extends AppCompatActivity {

    private static final String TAG = RegistrationActivity.class.getSimpleName();
    private static final int VERIFY_REQUEST_CODE = 2001;
    private Toast mToast;
    private ApiInterface apiService;
    private TextView registerBtn;
    private TextView goToLoginBtn;
    private Activity baseActivity;
    private TextInputEditText executiveName;
    private TextInputEditText executiveMobileNo;
    private TextInputEditText executiveLocation;
    private ImageView getExecutiveLocation;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    GPSTracker gps;
    private boolean gotAllPermissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        baseActivity = this;
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

//        gps = new GPSTracker(RegistrationActivity.this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        executiveName = findViewById(R.id.executiveName);
        executiveMobileNo = findViewById(R.id.executiveMobileNo);
        executiveLocation = findViewById(R.id.executiveLocation);
        getExecutiveLocation = findViewById(R.id.getExecutiveLocation);
        getExecutiveLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gotAllPermissions) {

                    if (gps.isGPSTurnedOn()) {
                        gps.getLocation();
                        final String lat = String.valueOf(gps.getLatitude());
                        final String longi = String.valueOf(gps.getLongitude());
                        executiveLocation.setText(lat + ", " + longi);
                    } else {
                        gps.showSettingsAlert();
                    }

                } else {
                    findUnAskedPermissions(permissions);
                }
            }
        });
        goToLoginBtn = findViewById(R.id.goToLogin);
        goToLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findUnAskedPermissions(permissions);
        registerBtn = findViewById(R.id.nextBtn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(executiveName.getText().toString().trim()))
                {
                    showToastAtCentre("Please enter your name", LENGTH_LONG);
                    return;
                }
                if (TextUtils.isEmpty(executiveMobileNo.getText().toString().trim()))
                {
                    showToastAtCentre("Please enter your mobile number", LENGTH_LONG);
                    return;
                }
                if (executiveMobileNo.getText().toString().trim().length() < 10 || executiveMobileNo.getText().toString().trim().length() >10)
                {
                    showToastAtCentre("Please enter a valid mobile number", LENGTH_LONG);
                    return;
                }
                if (TextUtils.isEmpty(executiveLocation.getText().toString().trim()))
                {
                    showToastAtCentre("Please select your location", LENGTH_LONG);
                    return;
                }
                String loc[] = executiveLocation.getText().toString().trim().split(",");
                if (loc.length == 2)
                {
                    doSignUp(executiveName.getText().toString().trim(),executiveMobileNo.getText().toString().trim(),loc[0].trim(),loc[1].trim());
                }
                else
                {
                    showToastAtCentre("Please enter a valid location", LENGTH_LONG);
                }
            }
        });
    }

    public void doSignUp(final String name, final String mobileNo, final String latitude, final String longitude)
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        Call<RegistrationModel> call = apiService.executiveRegistration(name, mobileNo, latitude, longitude);

        call.enqueue(new Callback<RegistrationModel>() {
            @Override
            public void onResponse(Call<RegistrationModel> call, Response<RegistrationModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
//                    int userId = response.body().getData();
//                    SharedPreferenceUtility.saveUserLoginDetails(getApplicationContext(),userId,mobileNo,name,latitude,longitude);
//                    Intent intent = getIntent();
//                    setResult(RESULT_OK, intent);
//                    finish();
                    Intent loginIntent = new Intent(baseActivity, VerifyPasswordActivity.class);
                    loginIntent.putExtra("mobileNo",mobileNo);
                    startActivityForResult(loginIntent, VERIFY_REQUEST_CODE);
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<RegistrationModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }

    private void findUnAskedPermissions(ArrayList wanted) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList result = new ArrayList();

            for (Object perm : wanted) {
                if (!hasPermission((String) perm)) {
                    result.add(perm);
                }
            }

            permissionsToRequest = result;

            if (permissionsToRequest.size() > 0) {
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            } else {
                gotAllPermissions = true;
                gps = new GPSTracker(RegistrationActivity.this);
                if (gps.isGPSTurnedOn()) {
                    gps.getLocation();
                    final String lat = String.valueOf(gps.getLatitude());
                    final String longi = String.valueOf(gps.getLongitude());
                    executiveLocation.setText(lat + ", " + longi);
                } else {
                    gps.showSettingsAlert();
                }
            }
        }

    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                permissionsRejected.clear();
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0).toString())) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                String[] arr = (String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]);
                                                requestPermissions(arr, ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                        }
                    }

                } else {
                    gotAllPermissions = true;
                    gps = new GPSTracker(RegistrationActivity.this);

                        if (gps.isGPSTurnedOn()) {
                            gps.getLocation();
                            final String lat = String.valueOf(gps.getLatitude());
                            final String longi = String.valueOf(gps.getLongitude());
                            executiveLocation.setText(lat + ", " + longi);
                        } else {
                            gps.showSettingsAlert();
                        }

                }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VERIFY_REQUEST_CODE && resultCode == RESULT_OK)
        {
            Intent intent = getIntent();
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(RegistrationActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}

package in.waycool.outgrow;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.dewinjm.monthyearpicker.MonthYearPickerDialog;
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.waycool.outgrow.Models.CropTypesModel;
import in.waycool.outgrow.Models.CropsModel;
import in.waycool.outgrow.Models.GeneralModel;
import in.waycool.outgrow.Retrofit.ApiClient;
import in.waycool.outgrow.Retrofit.ApiInterface;
import in.waycool.outgrow.Utils.UtilsClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;

public class AddCropActivity extends AppCompatActivity {

    private TextInputEditText startDate;
    private TextInputEditText endDate;
    private TextInputEditText totalCost;
    private TextInputEditText totalSaleAmount;
    private TextView submitCropDetails;
    private RelativeLayout bottomCropLayout;
    private ImageView goBack;
    private Spinner cropTypesSpinner;
    List<String> cropTypes = new ArrayList<>();
    private Activity baseActivity;
    private static final String TAG = AddCropActivity.class.getSimpleName();
    private Toast mToast;
    private ApiInterface apiService;
    private String selectedCropType;
    private String farmerId;
    private String screenSource;
    private ImageView editCropBtn;
    private ImageView deleteCropBtn;
    private TextView cropTitleTxt;
    private String cropId;
    private boolean isEditClicked;
    android.app.DatePickerDialog datePickerDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_crop);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        baseActivity = this;
        farmerId = getIntent().getStringExtra("farmer_id");
        cropId = getIntent().getStringExtra("crop_id");
        screenSource = getIntent().getStringExtra("source");
        Log.d(TAG, "Farmer Id: " + farmerId);
        cropTitleTxt = findViewById(R.id.cropTitle);
        bottomCropLayout = findViewById(R.id.bottomCropLayout);
        editCropBtn = findViewById(R.id.editCrop);
        deleteCropBtn = findViewById(R.id.deleteCrop);
        editCropBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEditClicked = true;
                editCropBtn.setVisibility(View.INVISIBLE);
                deleteCropBtn.setVisibility(View.INVISIBLE);
                startDate.setEnabled(true);
                endDate.setEnabled(true);
                totalCost.setEnabled(true);
                totalSaleAmount.setEnabled(true);
                cropTypesSpinner.setEnabled(true);
                submitCropDetails.setVisibility(View.VISIBLE);
                bottomCropLayout.setVisibility(View.VISIBLE);
            }
        });

        deleteCropBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDeleteCrop("Delete Crop Information", "Are you sure to delete?","Cancel", "Delete");
            }
        });

        cropTypesSpinner = findViewById(R.id.cropType);
        startDate = findViewById(R.id.startDate);
        startDate.setKeyListener(null);

        startDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    showDatePicker(true);
                }
            }
        });
        endDate = findViewById(R.id.endDate);
        endDate.setKeyListener(null);

        endDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    showDatePicker(false);
                }
            }
        });
        totalCost = findViewById(R.id.totalCost);
        totalSaleAmount = findViewById(R.id.totalSaleAmount);
        submitCropDetails = findViewById(R.id.submitCropDetails);
        submitCropDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String startDte = startDate.getText().toString().trim();
                String endDt = endDate.getText().toString().trim();
                String costAmount = totalCost.getText().toString().trim();
                String saleAmount = totalSaleAmount.getText().toString().trim();

                if (selectedCropType.equalsIgnoreCase("Select Crop"))
                {
                    showToastAtCentre("Select the Crop", LENGTH_SHORT);
                    return;
                }
                if (TextUtils.isEmpty(startDte))
                {
                    showToastAtCentre("Enter the start date", LENGTH_SHORT);
                    return;
                }
                if (TextUtils.isEmpty(endDt))
                {
                    showToastAtCentre("Enter the end date", LENGTH_SHORT);
                    return;
                }
                if (selectedStartDate.after(selectedEndDate))
                {
                    showToastAtCentre("Start date should not be after end date", LENGTH_SHORT);
                    return;
                }
                if (selectedStartDate.get(Calendar.MONTH)==selectedEndDate.get(Calendar.MONTH) && (selectedStartDate.get(Calendar.YEAR)==selectedEndDate.get(Calendar.YEAR)))
                {
                    showToastAtCentre("Start date and end date should not be the same", LENGTH_SHORT);
                    return;
                }
                if (TextUtils.isEmpty(costAmount))
                {
                    showToastAtCentre("Enter the total cost", LENGTH_SHORT);
                    return;
                }
                if (TextUtils.isEmpty(saleAmount))
                {
                    showToastAtCentre("Enter the total sale amount", LENGTH_SHORT);
                    return;
                }
                if (isEditClicked)
                {
                    updateCropDetails(cropId, selectedCropType, startDte, endDt, costAmount, saleAmount);
                }
                else {
                    saveCropDetails(farmerId, selectedCropType, startDte, endDt, costAmount, saleAmount);
                }

            }
        });
        if (screenSource.equalsIgnoreCase("add_crop")) {
            cropTitleTxt.setText("Enter Crop Details");
            editCropBtn.setVisibility(View.INVISIBLE);
            deleteCropBtn.setVisibility(View.INVISIBLE);
        } else {
            cropTitleTxt.setText("Crop Details");
            editCropBtn.setVisibility(View.VISIBLE);
            deleteCropBtn.setVisibility(View.VISIBLE);
            startDate.setEnabled(false);
            endDate.setEnabled(false);
            totalCost.setEnabled(false);
            totalSaleAmount.setEnabled(false);
            cropTypesSpinner.setEnabled(false);
            submitCropDetails.setVisibility(View.INVISIBLE);
            bottomCropLayout.setVisibility(View.INVISIBLE);
        }
        goBack = findViewById(R.id.backBtn);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getCropTypes();
    }

    private Calendar selectedStartDate;
    private Calendar selectedEndDate;
    public void showDatePicker(final boolean start)
    {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

        int yearSelected;
        int monthSelected;

        Calendar calendar = Calendar.getInstance();
        yearSelected = calendar.get(Calendar.YEAR);
        c.set(mYear, mMonth, mDay);
        long maxDate = c.getTimeInMillis();

        monthSelected = calendar.get(Calendar.MONTH);

        MonthYearPickerDialogFragment dialogFragment = MonthYearPickerDialogFragment
                .getInstance(monthSelected, yearSelected,0,maxDate);

        dialogFragment.setOnDateSetListener(new MonthYearPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int year, int monthOfYear) {
                // do something
                Log.d(TAG,"Month: " + monthOfYear);
                Log.d(TAG,"Year: " + year);


                Calendar isDateSelected = Calendar.getInstance();
                isDateSelected.set(year,monthOfYear, 1);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy");

                String dateString = sdf.format(isDateSelected.getTime());
                if (start)
                {
                    selectedStartDate = isDateSelected;
                    startDate.setText(dateString);
                }else
                {
                    selectedEndDate = isDateSelected;
                    endDate.setText(dateString);
                }
            }
        });
        dialogFragment.show(getSupportFragmentManager(), null);

    }

    public void loadCropData()
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        Call<CropsModel> call = apiService.getCropDetails(cropId);

        call.enqueue(new Callback<CropsModel>() {
            @Override
            public void onResponse(Call<CropsModel> call, Response<CropsModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    startDate.setText(response.body().getData().get(0).getStart_date());
                    endDate.setText(response.body().getData().get(0).getEnd_date());
                    totalCost.setText(response.body().getData().get(0).getTotal_cost());
                    totalSaleAmount.setText(response.body().getData().get(0).getTotal_sale_amount());
                    String crop = response.body().getData().get(0).getCrop_name().trim();
                    int index = 0;
                    for (String cropType: cropTypes) {
                        if (cropType.equalsIgnoreCase(crop))
                        {
                            cropTypesSpinner.setSelection(index);
                            break;
                        }
                        index++;
                    }
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<CropsModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);

            }
        });
    }

    public void saveCropDetails(String farmerID, String crop, String startDateStr, String endDateStr, String cost, String sale)
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        Call<GeneralModel> call = apiService.addCropData(farmerID, startDateStr, endDateStr,cost,sale,crop);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    Log.d(TAG, "Crop added Successfully!");
                    showToastAtCentre("Crop added Successfully",LENGTH_SHORT);
                    finish();
//                    cropTitleTxt.setText("Crop Details");
//                    editCropBtn.setVisibility(View.VISIBLE);
//                    deleteCropBtn.setVisibility(View.VISIBLE);
//                    startDate.setEnabled(false);
//                    endDate.setEnabled(false);
//                    totalCost.setEnabled(false);
//                    totalSaleAmount.setEnabled(false);
//                    cropTypesSpinner.setEnabled(false);
//                    submitCropDetails.setVisibility(View.INVISIBLE);
//                    bottomCropLayout.setVisibility(View.INVISIBLE);
////                    loadCropData();
                    if (screenSource.equalsIgnoreCase("crop_details"))
                    {
                        Intent intent = new Intent("update_crop_data");
                        sendBroadcast(intent);
                    }
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }

    public void updateCropDetails(String cropId, String crop, String startDateStr, String endDateStr, String cost, String sale)
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        Call<GeneralModel> call = apiService.updateCropData(cropId, startDateStr, endDateStr,cost,sale,crop);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    isEditClicked = false;
                    Log.d(TAG, "Crop Updated Successfully!");
                    showToastAtCentre("Crop Updated Successfully",LENGTH_SHORT);
                    finish();

//                    cropTitleTxt.setText("Crop Details");
//                    editCropBtn.setVisibility(View.VISIBLE);
//                    deleteCropBtn.setVisibility(View.VISIBLE);
//                    startDate.setEnabled(false);
//                    endDate.setEnabled(false);
//                    totalCost.setEnabled(false);
//                    totalSaleAmount.setEnabled(false);
//                    cropTypesSpinner.setEnabled(false);
//                    submitCropDetails.setVisibility(View.INVISIBLE);
//                    bottomCropLayout.setVisibility(View.INVISIBLE);
////                    loadCropData();
                    if (screenSource.equalsIgnoreCase("crop_details"))
                    {
                        Intent intent = new Intent("update_crop_data");
                        sendBroadcast(intent);
                    }
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }


    public void getCropTypes()
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        Call<CropTypesModel> call = apiService.getCropTypes();

        call.enqueue(new Callback<CropTypesModel>() {
            @Override
            public void onResponse(Call<CropTypesModel> call, Response<CropTypesModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    Log.d(TAG, "Login Success!");
                    cropTypes.clear();
                    cropTypes.add("Select Crop");
                    for (CropTypesModel.Crops crop : response.body().getData()) {
                        cropTypes.add(crop.getCrop_name());
                    }
                    if (screenSource.equalsIgnoreCase("crop_details"))
                    {
                        loadCropData();
                    }
                    setUpCropTypes();
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<CropTypesModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }

    private void setUpCropTypes()
    {
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(baseActivity, R.layout.spinner_item, cropTypes);
        cropTypesSpinner.setAdapter(dataAdapter);

        cropTypesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String data = dataAdapter.getItem(position);
                selectedCropType = data;
                Log.d(TAG, "Sorting Chosen: " + data);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }

    private void confirmDeleteCrop(String titleTxt, String titleDesc, String cancelBtnText , String okBtnText) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = dialog.findViewById(R.id.alertTitle1);
        TextView desc = dialog.findViewById(R.id.alertText);
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn  = dialog.findViewById(R.id.alertOk);

        title.setText(titleTxt);
        desc.setText(titleDesc);
        okBtn.setText(okBtnText);
        cancelBtn.setText(cancelBtnText);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                deleteCrop(cropId);
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void deleteCrop(String cropId) {

        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        // change the api
        Call<GeneralModel> call = apiService.deleteCrop(cropId);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    showToastAtCentre("Crop deleted successfully", LENGTH_LONG);
                    if (screenSource.equalsIgnoreCase("crop_details"))
                    {
                        Intent intent = new Intent("update_crop_data");
                        sendBroadcast(intent);
                    }
                    finish();

                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }
}

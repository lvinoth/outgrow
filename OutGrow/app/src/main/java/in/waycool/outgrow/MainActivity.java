package in.waycool.outgrow;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mukesh.OtpView;

import java.io.IOException;

import in.waycool.outgrow.Models.GetOTPModel;
import in.waycool.outgrow.Retrofit.ApiClient;
import in.waycool.outgrow.Retrofit.ApiInterface;
import in.waycool.outgrow.Utils.UtilsClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends AppCompatActivity {

    private static final int SIGNUP_REQUEST_CODE = 101;
    private static final int SIGNIN_REQUEST_CODE = 102;
    private static final int SMS_PERMISSION_CODE = 1001;
    private TextView loginBtn;
    private Activity baseActivity;
    private TextView register;
    private OtpView mobileNumber;
    private static final String TAG = MainActivity.class.getSimpleName();
    private Toast mToast;
    private ApiInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        baseActivity = this;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        mobileNumber = findViewById(R.id.otp_view);
        loginBtn = findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int len = mobileNumber.getText().toString().length();
                if (len  == 0)
                {
                    showToastAtCentre("Please enter your mobile number", LENGTH_LONG);
                    return;
                }
                if (len < 10 || len >10)
                {
                    showToastAtCentre("Please enter a valid mobile number", LENGTH_LONG);
                    return;
                }
                getOtp(mobileNumber.getText().toString());
//                startActivity(new Intent(baseActivity,DrawerActivity.class));
            }
        });
        if (!isSmsPermissionGranted())
        {
            requestReadAndSendSmsPermission();
        }
        register = findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(baseActivity, RegistrationActivity.class), SIGNUP_REQUEST_CODE);
            }
        });
    }

    public void getOtp(final String mobileNo)
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        Call<GetOTPModel> call = apiService.getOTP(mobileNo);

        call.enqueue(new Callback<GetOTPModel>() {
            @Override
            public void onResponse(Call<GetOTPModel> call, Response<GetOTPModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    Intent loginIntent = new Intent(baseActivity, VerifyPasswordActivity.class);
                    loginIntent.putExtra("mobileNo",mobileNo);
                    startActivityForResult(loginIntent, SIGNIN_REQUEST_CODE);
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<GetOTPModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SIGNUP_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {

//                    startActivity(new Intent(getApplicationContext(), DrawerActivity.class));
                    finish();
                }
                break;
            case SIGNIN_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {

//                    startActivity(new Intent(getApplicationContext(), DrawerActivity.class));
                    finish();
                }
                break;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case SMS_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                }
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public boolean isSmsPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Request runtime SMS permission
     */
    private void requestReadAndSendSmsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)) {
            // You may display a non-blocking explanation here, read more in the documentation:
            // https://developer.android.com/training/permissions/requesting.html
        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS,Manifest.permission.RECEIVE_SMS}, SMS_PERMISSION_CODE);
    }
}

package in.waycool.outgrow.Retrofit;

import in.waycool.outgrow.Models.AddFarmerModel;
import in.waycool.outgrow.Models.CropTypesModel;
import in.waycool.outgrow.Models.CropsModel;
import in.waycool.outgrow.Models.FarmerDetailsModel;
import in.waycool.outgrow.Models.FarmersModel;
import in.waycool.outgrow.Models.GeneralModel;
import in.waycool.outgrow.Models.GetOTPModel;
import in.waycool.outgrow.Models.HistoryModel;
import in.waycool.outgrow.Models.LoginModel;
import in.waycool.outgrow.Models.RegistrationModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("field_executive_registration")
    Call<RegistrationModel> executiveRegistration(@Field("fe_name") String name, @Field("fe_mobile") String mobileNo,
                                                  @Field("fe_latitude") String latitude, @Field("fe_longitude") String longitude);

    @FormUrlEncoded
    @POST("send_otp")
    Call<GetOTPModel> getOTP(@Field("fe_mobile") String mobileNo);

    @FormUrlEncoded
    @POST("authorize_login")
    Call<LoginModel> executiveLogin(@Field("fe_mobile") String mobileNo, @Field("otp") String otp);

    @FormUrlEncoded
    @POST("farmer_list")
    Call<FarmersModel> getFarmersList(@Field("field_executive_id") String executiveId, @Field("requset_type") String type, @Field("from_date") String fromDate, @Field("to_date") String toDate);

    @FormUrlEncoded
    @POST("add_crop")
    Call<GeneralModel> addCropData(@Field("farmer_id") String farmerId, @Field("crop_start_date") String cropStartDate,
                                   @Field("crop_end_date") String cropEndDate, @Field("total_cost") String totalCost,
                                   @Field("total_sale_amount") String totalSaleAmount, @Field("crop_name") String cropName);

    @FormUrlEncoded
    @POST("update_crop_details")
    Call<GeneralModel> updateCropData(@Field("crop_details_id") String farmerId, @Field("crop_start_date") String cropStartDate,
                                   @Field("crop_end_date") String cropEndDate, @Field("total_cost") String totalCost,
                                   @Field("total_sale_amount") String totalSaleAmount, @Field("crop_name") String cropName);

    @Multipart
    @POST("add_farmer")
    Call<AddFarmerModel> addFarmerData(@Part("farmer_name") RequestBody farmerName,
                                       @Part("farmer_mobile") RequestBody farmerMobile, @Part("farmer_latitude") RequestBody farmerLatitude,
                                       @Part("farmer_longitude") RequestBody farmerLongitude, @Part("farmer_village") RequestBody farmerVillage,
                                       @Part("farmer_state") RequestBody farmerState, @Part("farmer_landarea") RequestBody farmerLandArea,
                                       @Part("field_executive_id") RequestBody executiveId,@Part("soil_sample") RequestBody soilSample,
                                       @Part MultipartBody.Part userFile);

    @Multipart
    @POST("update_farmer")
    Call<GeneralModel> updateFarmerData(@Part("farmer_name") RequestBody farmerName,
                                       @Part("farmer_mobile") RequestBody farmerMobile, @Part("farmer_latitude") RequestBody farmerLatitude,
                                       @Part("farmer_longitude") RequestBody farmerLongitude, @Part("farmer_village") RequestBody farmerVillage,
                                       @Part("farmer_state") RequestBody farmerState, @Part("farmer_landarea") RequestBody farmerLandArea,
                                       @Part("farmer_id") RequestBody executiveId,@Part("soil_sample") RequestBody soilSample,
                                       @Part MultipartBody.Part userFile);

    @FormUrlEncoded
    @POST("crop_list")
    Call<CropsModel> getAddedCropList(@Field("farmer_id") String farmerId);

    @FormUrlEncoded
    @POST("farmer_details")
    Call<FarmerDetailsModel> getFarmerDetails(@Field("farmer_id") String farmerId);

    @FormUrlEncoded
    @POST("crop_details")
    Call<CropsModel> getCropDetails(@Field("crop_details_id") String cropId);

    @FormUrlEncoded
    @POST("delete_farmer")
    Call<GeneralModel> deleteFarmer(@Field("farmer_id") String farmerId);

    @FormUrlEncoded
    @POST("delete_crop_details")
    Call<GeneralModel> deleteCrop(@Field("crop_details_id") String cropId);

    @FormUrlEncoded
    @POST("my_history")
    Call<HistoryModel> getHistoryList(@Field("field_executive_id") String executiveId, @Field("requset_type") String requestType,
                                      @Field("from_month") String fromMonth, @Field("to_month") String toMonth);

    @GET("crop_types")
    Call<CropTypesModel> getCropTypes();

    @Multipart
    @POST("upload_profile_picture")
    Call<GeneralModel> uploadExecutiveImage(@Part("field_executive_id") RequestBody farmerName,
                                       @Part MultipartBody.Part userFile);


}

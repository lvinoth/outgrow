package in.waycool.outgrow.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import in.waycool.outgrow.AddFarmerActivity;
import in.waycool.outgrow.DrawerActivity;
import in.waycool.outgrow.Models.FarmersModel;
import in.waycool.outgrow.Models.GeneralModel;
import in.waycool.outgrow.R;
import in.waycool.outgrow.Retrofit.ApiClient;
import in.waycool.outgrow.Retrofit.ApiInterface;
import in.waycool.outgrow.Utils.UtilsClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class FarmersAdapter extends RecyclerView.Adapter<FarmersAdapter.ViewHolder>  {

    private static final String TAG = FarmersAdapter.class.getSimpleName();
    private boolean isToday;
    private List<FarmersModel.Farmers> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private Activity baseActivity;
    private ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private FarmerUpdateInterface farmerUpdateInterface;
    private Toast mToast;

    public interface FarmerUpdateInterface {
        void onFarmerDataUpdated();
    }

    // data is passed into the constructor
    public FarmersAdapter(Context context, List<FarmersModel.Farmers> data, boolean isToday) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.isToday = isToday;
        this.mContext = context;
        this.baseActivity = (DrawerActivity)context;
        this.farmerUpdateInterface = (FarmerUpdateInterface) context;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public FarmersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.farmer_details_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FarmersModel.Farmers farmer = mData.get(position);
        holder.farmerName.setText(farmer.getFarmer_name());
        holder.farmerId.setText(farmer.getFarmer_unique_id());
        holder.farmerLocation.setText(farmer.getVillage_name() +", "  + farmer.getState());
        holder.farmerLandArea.setText(farmer.getLand_area() + " Acre(s)");
        holder.isFarmerCompleted.setVisibility(farmer.getIs_crop_added().equalsIgnoreCase("1") ? View.GONE : View.VISIBLE );
        final String farmerImagePath = farmer.getFarmer_image();

        final float scale = mContext.getResources().getDisplayMetrics().density;
        // convert the DP into pixel
        int width = (int)(65 * scale + 0.5f);

        if (TextUtils.isEmpty(farmerImagePath)) {
//            TODO: Vinoth // Place a error image in nodpi folder and render

//            Picasso.with(mContext).load(R.drawable.outgrow_logo_final_01)
//                    .placeholder(R.drawable.outgrow_logo_final_01)
//                    .error(R.drawable.outgrow_logo_final_01)
//                    .into(holder.farmerImage);
        }
        else{
            Picasso.with(mContext)
                    .load(farmerImagePath)
                    .resize(width, width)
                    .centerInside()
                    .onlyScaleDown()
                    .into(holder.farmerImage);
        }

        holder.farmerDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmDeleteFarmer("Delete Farmer Information", "Are you sure to delete?","Cancel", "Delete", farmer.getFarmer_id());
            }
        });

        holder.farmerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean cropAdd;
                if (farmer.getIs_crop_added().equalsIgnoreCase("1"))
                {
                    // Crop add in all days
                    cropAdd = true;
                }
                else { // crop not added
                    // if it is today, let edit
                    if (isToday) {
                        cropAdd = false;
                    } // otherwise dont
                    else {
                        cropAdd = true;
                    }
                }
                Intent farmerDetail = new Intent(mContext.getApplicationContext(), AddFarmerActivity.class);
                farmerDetail.putExtra("farmer_id",farmer.getFarmer_id().trim());
                farmerDetail.putExtra("source", "farmer_details");
                farmerDetail.putExtra("isToday", isToday);
                farmerDetail.putExtra("crops_added", cropAdd);
                mContext.startActivity(farmerDetail);
            }
        });

    }

    public void deleteFarmer(String farmerId)
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        Call<GeneralModel> call = apiService.deleteFarmer(farmerId);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    showToastAtCentre("Farmer deleted successfully", LENGTH_LONG);
                    farmerUpdateInterface.onFarmerDataUpdated();
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(mContext, message, duration);
        mToast.show();
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView farmerImage;
        ImageView farmerDelete;
        RelativeLayout farmerLayout;
        TextView farmerName;
        TextView farmerId;
        TextView farmerLocation;
        TextView farmerLandArea;
        TextView isFarmerCompleted;


        ViewHolder(View itemView) {
            super(itemView);
            farmerImage = itemView.findViewById(R.id.farmerImage);
            farmerDelete = itemView.findViewById(R.id.deleteFarmer);
            farmerName = itemView.findViewById(R.id.farmerName);
            farmerId = itemView.findViewById(R.id.farmerId);
            farmerLandArea = itemView.findViewById(R.id.farmerLandArea);
            farmerLocation = itemView.findViewById(R.id.farmerLocation);
            farmerLayout = itemView.findViewById(R.id.childLayout);
            isFarmerCompleted = itemView.findViewById(R.id.isFarmerCompleted);

            farmerDelete.setVisibility(isToday ? View.VISIBLE : View.INVISIBLE);

            farmerImage.setClipToOutline(true);
            farmerDelete.setClipToOutline(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
//
//    private void setDiffTypeFace(String convertedMoney, TextView view)    {
//        String convertedMoneyString = "₹ " + convertedMoney;
//        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/poppins_bold.otf");
//        SpannableStringBuilder SS = new SpannableStringBuilder(convertedMoneyString);
//        SS.setSpan (new CustomTypefaceSpan("", font), 0, 2, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
//        view.setText(SS);
//    }

    // convenience method for getting data at click position
    FarmersModel.Farmers getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    private void confirmDeleteFarmer(String titleTxt, String titleDesc, String cancelBtnText , String okBtnText, final String farmerId) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = dialog.findViewById(R.id.alertTitle1);
        TextView desc = dialog.findViewById(R.id.alertText);
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn  = dialog.findViewById(R.id.alertOk);

        title.setText(titleTxt);
        desc.setText(titleDesc);
        okBtn.setText(okBtnText);
        cancelBtn.setText(cancelBtnText);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                deleteFarmer(farmerId);
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }
}

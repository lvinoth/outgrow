package in.waycool.outgrow.Models;

import java.util.List;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class FarmersModel {


    private int status;

    private String message;

    private List<Farmers> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Farmers> getData() {
        return data;
    }

    public void setData(List<Farmers> data) {
        this.data = data;
    }


    public class Farmers
    {
        private String farmer_id;
        private String farmer_unique_id;
        private String farmer_name;
        private String farmer_mobile;
        private String farmer_latitude;
        private String farmer_longitude;
        private String village_name;
        private String state;
        private String land_area;
        private String farmer_image;
        private String soil_sample;
        private String is_crop_added;

        public String getFarmer_id() {
            return farmer_id;
        }

        public void setFarmer_id(String farmer_id) {
            this.farmer_id = farmer_id;
        }

        public String getFarmer_unique_id() {
            return farmer_unique_id;
        }

        public void setFarmer_unique_id(String farmer_unique_id) {
            this.farmer_unique_id = farmer_unique_id;
        }

        public String getFarmer_name() {
            return farmer_name;
        }

        public void setFarmer_name(String farmer_name) {
            this.farmer_name = farmer_name;
        }

        public String getFarmer_mobile() {
            return farmer_mobile;
        }

        public void setFarmer_mobile(String farmer_mobile) {
            this.farmer_mobile = farmer_mobile;
        }

        public String getFarmer_latitude() {
            return farmer_latitude;
        }

        public void setFarmer_latitude(String farmer_latitude) {
            this.farmer_latitude = farmer_latitude;
        }

        public String getFarmer_longitude() {
            return farmer_longitude;
        }

        public void setFarmer_longitude(String farmer_longitude) {
            this.farmer_longitude = farmer_longitude;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getLand_area() {
            return land_area;
        }

        public void setLand_area(String land_area) {
            this.land_area = land_area;
        }

        public String getFarmer_image() {
            return farmer_image;
        }

        public void setFarmer_image(String farmer_image) {
            this.farmer_image = farmer_image;
        }

        public String getIs_crop_added() {
            return is_crop_added;
        }

        public void setIs_crop_added(String is_crop_added) {
            this.is_crop_added = is_crop_added;
        }

        public String getSoil_sample() {
            return soil_sample;
        }

        public void setSoil_sample(String soil_sample) {
            this.soil_sample = soil_sample;
        }
    }

}

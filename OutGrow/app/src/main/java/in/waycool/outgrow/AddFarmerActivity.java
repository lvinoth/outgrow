package in.waycool.outgrow;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.waycool.outgrow.Adapters.CropsAdapter;
import in.waycool.outgrow.Models.AddFarmerModel;
import in.waycool.outgrow.Models.CropsModel;
import in.waycool.outgrow.Models.FarmerDetailsModel;
import in.waycool.outgrow.Models.GeneralModel;
import in.waycool.outgrow.Retrofit.ApiClient;
import in.waycool.outgrow.Retrofit.ApiInterface;
import in.waycool.outgrow.Utils.GPSTracker;
import in.waycool.outgrow.Utils.SharedPreferenceUtility;
import in.waycool.outgrow.Utils.UtilsClass;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;

public class AddFarmerActivity extends AppCompatActivity implements CropsAdapter.CropUpdateInterface {

    private static final String TAG = AddFarmerActivity.class.getSimpleName();
    private static final int ADD_CROP_REQUEST = 1002;
    private TextView addCropText;
    private Activity baseActivity;
    private ImageView farmerImage;
    private ImageView goBack;
    private Toast mToast;
    private ApiInterface apiService;
    private RelativeLayout bottomLayout;
    private LinearLayout checkBoxLayout;
    private String[] stateList;

    private TextInputEditText farmerName;
    private TextInputEditText farmerMobile;
    private TextInputEditText farmerLocation;
    private TextInputEditText farmerVillage;
    private TextInputEditText farmerLandArea;
    private TextInputEditText farmerState;
//    private String farmerStateSelected = "Select State";
    private TextView submitFarmerData;
    private TextView farmerTitle;

    private RecyclerView cropsRecyclerView;
    private List<CropsModel.CropData> cropsList;
    private CropsAdapter adapter;
    private ImageView takeAPic;
    private String farmerId;
    private String farmerIdForDetails;
    private ImageView editBtn;
    private ImageView deleteBtn;
    private ImageView getLocation;
    private String screenSource;
    private TextView saveFarmerDetailsBtn;
    private TextView cancelFarmerDetails;
    private boolean isEditClicked;
    private boolean isSourceAddFarmer;
    private boolean isAllCropsAdded;

    private String tempFName;
    private String tempFMobile;
    private String tempFLocation;
    private String tempFVillage;
    private String tempFState;
    private String tempFLand;


    private String userChoosenTask = "";
    private static final int REQUEST_CAMERA = 101;
    private static final int SELECT_FILE = 102;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1234;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 456;
    private Uri outputFileUri;
    private File farmerProfilePicture;
    private Spinner farmerStateSpinner;
    private TextView farmerUniqueId;
    private CheckBox haveCollectedCropCheckbox;
    private boolean isToday;

    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    GPSTracker gps;
    private boolean gotAllPermissions;
    private boolean isFarmerLocallySaved;
    private boolean isSoilSampleCollected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_farmer);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        baseActivity = this;
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        farmerStateSpinner = findViewById(R.id.statesSpinner);
        farmerStateSpinner.setEnabled(false);
        saveFarmerDetailsBtn = findViewById(R.id.saveFarmerDetails);
        cancelFarmerDetails = findViewById(R.id.cancelFarmerDetails);
        bottomLayout = findViewById(R.id.bottomLayout);
        checkBoxLayout = findViewById(R.id.checkBoxLayout);
        farmerUniqueId = findViewById(R.id.farmerUniqueId);
        registerReceiver(broadcastReceiver, new IntentFilter("update_crop_data"));
        cancelFarmerDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (isFarmerLocallySaved && cropsList.size() < 3)
//                {
//                    final Dialog dialog = new Dialog(baseActivity);
//                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                    dialog.setContentView(R.layout.custom_alert);
//
//                    WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();
//
//                    wlp.gravity = Gravity.CENTER;
//                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
//                    dialog.getWindow().setAttributes(wlp);
//                    dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                            WindowManager.LayoutParams.WRAP_CONTENT);
//
//                    TextView title = dialog.findViewById(R.id.alertTitle1);
//                    TextView desc = dialog.findViewById(R.id.alertText);
//                    TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
//                    TextView okBtn = dialog.findViewById(R.id.alertOk);
//
//                    title.setText("Delete Farmer Information");
//                    desc.setText("Cancelling without minimum of 3 crops details will delete the farmer data permanently. Do you want to continue?");
//                    okBtn.setText("Delete");
//                    cancelBtn.setText("Cancel");
//
//                    cancelBtn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.cancel();
//                        }
//                    });
//
//                    okBtn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            // Invoke Delete API here
//                            deleteFarmer(farmerId);
//                            dialog.cancel();
//                        }
//                    });
//
//                    dialog.setCanceledOnTouchOutside(false);
//                    dialog.setCancelable(false);
//                    dialog.show();
//                }
                if (isFarmerLocallySaved && cropsList.size() < 3)
                {
                    confirmDeleteFarmer("Delete Farmer Information", "Cancelling without minimum of 3 crops details will delete the farmer data permanently. Do you want to continue?", "Cancel", "Delete");
                }
                else if (isFarmerLocallySaved && cropsList.size() >= 3)
                {
                    confirmReSubmit();
                }
                else {
                    finish();
                }
//                else {
//
//                    farmerName.setText(tempFName);
//                    farmerMobile.setText(tempFMobile);
//                    farmerLocation.setText(tempFLocation);
//                    farmerVillage.setText(tempFVillage);
//                    farmerLandArea.setText(tempFLand);
//                    int index = 0;
//                    for (String state : stateList) {
//                        if (state.equalsIgnoreCase(tempFState)) {
//                            farmerStateSpinner.setSelection(index);
//                            break;
//                        }
//                        index++;
//                    }
//                    isEditClicked = false;
//                    isFarmerLocallySaved = false;
//                    adapter.setDeleteVisible(false);
//                    adapter.notifyDataSetChanged();
//                    cancelFarmerDetails.setVisibility(View.GONE);
//                    takeAPic.setVisibility(View.INVISIBLE);
//                    saveFarmerDetailsBtn.setVisibility(View.INVISIBLE);
//                    submitFarmerData.setVisibility(View.INVISIBLE);
//                    addCropText.setVisibility(View.GONE);
//                    deleteBtn.setVisibility(isToday ? View.VISIBLE : View.INVISIBLE);
//                    editBtn.setVisibility(isToday ? View.VISIBLE : View.INVISIBLE);
//                    bottomLayout.setVisibility(View.GONE);
//                    checkBoxLayout.setVisibility(View.VISIBLE);
//                    getLocation.setEnabled(false);
//                    farmerName.setEnabled(false);
//                    farmerMobile.setEnabled(false);
//                    farmerLocation.setEnabled(false);
//                    farmerLandArea.setEnabled(false);
//                    farmerVillage.setEnabled(false);
//                    farmerStateSpinner.setEnabled(false);
//                }
            }
        });
        editBtn = findViewById(R.id.editFarmer);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tempFName = farmerName.getText().toString().trim();
                tempFMobile = farmerMobile.getText().toString().trim();
                tempFLocation = farmerLocation.getText().toString().trim();
                tempFVillage = farmerVillage.getText().toString().trim();
                tempFState = farmerState.getText().toString().trim();//farmerStateSelected.trim();
                tempFLand = farmerLandArea.getText().toString().trim();
                isFarmerLocallySaved = true;
                isEditClicked = true;
                adapter.setDeleteVisible(true);
                adapter.notifyDataSetChanged();
                cancelFarmerDetails.setVisibility(View.VISIBLE);
                deleteBtn.setVisibility(View.INVISIBLE);
                editBtn.setVisibility(View.INVISIBLE);
                takeAPic.setVisibility(View.VISIBLE);
                checkBoxLayout.setVisibility(View.INVISIBLE);
                getLocation.setEnabled(true);
                farmerName.setEnabled(true);
                farmerMobile.setEnabled(true);
                farmerLocation.setEnabled(true);
                farmerState.setEnabled(true);
                farmerVillage.setEnabled(true);
//                farmerStateSpinner.setEnabled(true);
                farmerLandArea.setEnabled(true);
                saveFarmerDetailsBtn.setVisibility(View.INVISIBLE);
                submitFarmerData.setVisibility(View.VISIBLE);
                bottomLayout.setVisibility(View.VISIBLE);
                addCropText.setVisibility(View.VISIBLE);
            }
        });
        haveCollectedCropCheckbox = findViewById(R.id.haveCollectedCropCheckbox);
        deleteBtn = findViewById(R.id.deleteFarmer);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDeleteFarmer("Delete Farmer Information", "Are you sure to delete?", "Cancel", "Delete");
            }
        });
        farmerTitle = findViewById(R.id.farmerTitle);
        farmerName = findViewById(R.id.farmerName);
        farmerMobile = findViewById(R.id.farmerMobileNo);
        farmerLocation = findViewById(R.id.farmerLocation);
        farmerLocation.setKeyListener(null);
        getLocation = findViewById(R.id.getLocation);
        getLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gotAllPermissions) {
                    if (gps.isGPSTurnedOn()) {
                        gps.getLocation();
                        final String lat = String.valueOf(gps.getLatitude());
                        final String longi = String.valueOf(gps.getLongitude());
                        farmerLocation.setText(lat + ", " + longi);
                        Geocoder gcd = new Geocoder(baseActivity, Locale.getDefault());
                        List<Address> addresses = null;
                        try {
                            addresses = gcd.getFromLocation(Double.valueOf(lat), Double.valueOf(longi), 1);
                            if (addresses.size() > 0) {
                                Log.d(TAG, "State Name: " + addresses.get(0).getAdminArea());
                                String state = addresses.get(0).getAdminArea().trim();
                                farmerState.setText(state);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        gps.showSettingsAlert();
                    }

                } else {
                    findUnAskedPermissions(permissions);
                }
            }
        });
        farmerVillage = findViewById(R.id.farmerVillage);
        farmerLandArea = findViewById(R.id.farmerTotalLandInAcres);
        farmerState = findViewById(R.id.farmerState);
        farmerState.setKeyListener(null);
        submitFarmerData = findViewById(R.id.submitFarmerDetails);
        submitFarmerData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cropsList.size() < 3) {
                    showToastAtCentre("Minimum of 3 crops should be entered to submit farmer data", LENGTH_LONG);
                    return;
                }

                if (isSoilSampleCollected)
                {
                    confirmSubmitWithOUTSoilData("Are you sure to resubmit?","Resubmit Farmer Information?", "Cancel","Submit");
                }
                else {
                    confirmSubmitWithSoilData("Submit Farmer Information","Have you collected the soil sample?", "Cancel","Submit");
                }

            }
        });
        saveFarmerDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkFarmerDataAndSave(true, 0);
            }
        });
        farmerId = getIntent().getStringExtra("farmer_id");
        isToday = getIntent().getBooleanExtra("isToday", false);
        setUpStates();
        findUnAskedPermissions(permissions);
        goBack = findViewById(R.id.backBtn);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFarmerLocallySaved && cropsList.size() < 3)
                {
                    confirmDeleteFarmer("Delete Farmer Information", "Cancelling without minimum of 3 crops details will delete the farmer data permanently. Do you want to continue?", "Cancel", "Delete");
                }
                else if (isFarmerLocallySaved && cropsList.size() >= 3)
                {
                    confirmReSubmit();
                }
                else {
                    finish();
                }
            }
        });
        takeAPic = findViewById(R.id.takeAPic);
        takeAPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageUploadDialog();
            }
        });
        farmerImage = findViewById(R.id.farmer_image);
        farmerImage.setClipToOutline(true);
        addCropText = findViewById(R.id.addCrop);
        addCropText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent cropIntent = new Intent(baseActivity, AddCropActivity.class);
                cropIntent.putExtra("farmer_id", farmerId);
                cropIntent.putExtra("crop_id", "");
                cropIntent.putExtra("source", "add_crop");
                startActivityForResult(cropIntent, ADD_CROP_REQUEST);
            }
        });

        cropsRecyclerView = findViewById(R.id.cropsRecyclerView);
        cropsList = new ArrayList<>();
        adapter = new CropsAdapter(baseActivity, cropsList, "", isToday);

        screenSource = getIntent().getStringExtra("source");

        if (screenSource.equalsIgnoreCase("add_farmer")) {
            isEditClicked = true;
            isSourceAddFarmer = true;
            farmerTitle.setText(getResources().getString(R.string.new_entry));
            editBtn.setVisibility(View.INVISIBLE);
            deleteBtn.setVisibility(View.INVISIBLE);
            saveFarmerDetailsBtn.setVisibility(View.VISIBLE);
            submitFarmerData.setVisibility(View.INVISIBLE);
            cancelFarmerDetails.setVisibility(View.GONE);
            addCropText.setVisibility(View.VISIBLE);
            addCropText.setEnabled(false);
            addCropText.setTextColor(Color.parseColor("#a3a3a3"));
            checkBoxLayout.setVisibility(View.GONE);
            bottomLayout.setVisibility(View.VISIBLE);
            takeAPic.setVisibility(View.VISIBLE);
            getLocation();
        } else {
            boolean cropsAdded = getIntent().getBooleanExtra("crops_added",false);
            loadFarmerData();
            if (cropsAdded) {
                isEditClicked = false;
                farmerTitle.setText(getResources().getString(R.string.farmer_info));
                takeAPic.setVisibility(View.INVISIBLE);
                editBtn.setVisibility(isToday ? View.VISIBLE : View.INVISIBLE);
                deleteBtn.setVisibility(isToday ? View.VISIBLE : View.INVISIBLE);
                checkBoxLayout.setVisibility(View.VISIBLE);
                getLocation.setEnabled(false);
                farmerName.setEnabled(false);
                farmerMobile.setEnabled(false);
                farmerLocation.setEnabled(false);
                farmerState.setEnabled(false);
                farmerVillage.setEnabled(false);
//            farmerStateSpinner.setEnabled(false);
                farmerLandArea.setEnabled(false);
                saveFarmerDetailsBtn.setVisibility(View.INVISIBLE);
                submitFarmerData.setVisibility(View.INVISIBLE);
                cancelFarmerDetails.setVisibility(View.GONE);
                bottomLayout.setVisibility(View.GONE);
                addCropText.setVisibility(View.GONE);
            }
            else {
                isFarmerLocallySaved = true;
                isEditClicked = true;
                adapter.setDeleteVisible(true);
                adapter.notifyDataSetChanged();
                farmerTitle.setText(getResources().getString(R.string.farmer_info));
                takeAPic.setVisibility(View.VISIBLE);
                editBtn.setVisibility(View.INVISIBLE);
                deleteBtn.setVisibility(View.INVISIBLE);
                checkBoxLayout.setVisibility(View.GONE);
                getLocation.setEnabled(true);
                farmerName.setEnabled(true);
                farmerMobile.setEnabled(true);
                farmerLocation.setEnabled(true);
                farmerState.setEnabled(true);
                farmerVillage.setEnabled(true);
//            farmerStateSpinner.setEnabled(false);
                farmerLandArea.setEnabled(true);
                saveFarmerDetailsBtn.setVisibility(View.INVISIBLE);
                submitFarmerData.setVisibility(View.VISIBLE);
                cancelFarmerDetails.setVisibility(View.VISIBLE);
                bottomLayout.setVisibility(View.VISIBLE);
                addCropText.setVisibility(View.VISIBLE);
            }

        }
    }

    private void getLocation() {
        if (gotAllPermissions) {

            if (gps.isGPSTurnedOn()) {
                gps.getLocation();
                final String lat = String.valueOf(gps.getLatitude());
                final String longi = String.valueOf(gps.getLongitude());
                farmerLocation.setText(lat + ", " + longi);
                Geocoder gcd = new Geocoder(baseActivity, Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = gcd.getFromLocation(Double.valueOf(lat), Double.valueOf(longi), 1);
                    if (addresses.size() > 0) {
                        Log.d(TAG, "State Name: " + addresses.get(0).getAdminArea());
                        String state = addresses.get(0).getAdminArea().trim();
                        farmerState.setText(state);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (isFarmerLocallySaved && cropsList.size() < 3)
        {
            confirmDeleteFarmer("Delete Farmer Information", "Cancelling without minimum of 3 crops details will delete the farmer data permanently. Do you want to continue?", "Cancel", "Delete");
        }
        else if (isFarmerLocallySaved && cropsList.size() >= 3)
        {
            confirmReSubmit();
        }
        else {
            finish();
        }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action.equals("update_crop_data")) {
                getCropsList(farmerId);
            }
        }
    };

    private void setUpStates() {
        stateList = getResources().getStringArray(R.array.states_list);
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(baseActivity, R.layout.spinner_item, stateList);
        farmerStateSpinner.setAdapter(dataAdapter);

        farmerStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String data = dataAdapter.getItem(position);
//                farmerStateSelected = data;
                Log.d(TAG, "Sorting Chosen: " + data);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void checkFarmerDataAndSave(boolean isTemp, int isSoilCollected) {
        String fName = farmerName.getText().toString().trim();
        String fMobile = farmerMobile.getText().toString().trim();
        String fLocation = farmerLocation.getText().toString().trim();
        String fVillage = farmerVillage.getText().toString().trim();
        String fLand = farmerLandArea.getText().toString().trim();
        String fState = farmerState.getText().toString().trim();

//        if (farmerStateSelected.equalsIgnoreCase("Select State")) {
//            showToastAtCentre("Select the state", LENGTH_SHORT);
//            return;
//        }
        if (TextUtils.isEmpty(fName)) {
            showToastAtCentre("Enter farmer name", LENGTH_SHORT);
            return;
        }
        if (TextUtils.isEmpty(fMobile)) {
            showToastAtCentre("Enter farmer mobile number", LENGTH_SHORT);
            return;
        }
        if (fMobile.length() < 10 || fMobile.length() > 10) {
            showToastAtCentre("Enter a valid mobile number", LENGTH_SHORT);
            return;
        }
        if (TextUtils.isEmpty(fLocation)) {
            showToastAtCentre("Enter farmer location", LENGTH_SHORT);
            return;
        }
        if (TextUtils.isEmpty(fVillage)) {
            showToastAtCentre("Enter farmer village", LENGTH_SHORT);
            return;
        }
        if (TextUtils.isEmpty(fState)) {
            showToastAtCentre("Enter farmer state", LENGTH_SHORT);
            return;
        }
        if (TextUtils.isEmpty(fLand)) {
            showToastAtCentre("Enter the farmer's total land", LENGTH_SHORT);
            return;
        }
        double landInDouble = 0.0;
        try {
            landInDouble = Double.valueOf(fLand);
        }catch (NumberFormatException ex)
        {
            showToastAtCentre("Enter a valid value for farmer land", LENGTH_SHORT);
            return;
        }
        if (landInDouble > 10.0) {
            showToastAtCentre("Farmer's land should not exceed 10 acres", LENGTH_SHORT);
            return;
        }
        String loc[] = fLocation.trim().split(",");

        if (loc.length == 2)
        {
            if (!isTemp) {
                updateFarmerDetails(fName, fMobile, loc[0].trim(), loc[1].trim(), fState, fVillage, fLand, isSoilCollected);
            } else {
                saveFarmerDetails(fName, fMobile, loc[0].trim(), loc[1].trim(), fState, fVillage, fLand, isSoilCollected);
            }
        }
        else
        {
            showToastAtCentre("Please enter a valid location", LENGTH_LONG);
        }
    }


    public void saveFarmerDetails(String fName, String fMobile, String fLatitude, String fLongitude, String farmerState, String fVillage, String fLand, int isSoilCollected) {
        UtilsClass.showBusyAnimation(baseActivity, "Registering..");
        int exeID = SharedPreferenceUtility.getUserId(baseActivity);
        RequestBody requestFile = null;
        MultipartBody.Part uploadbody = null;

        if (farmerProfilePicture != null) {
            requestFile = RequestBody.create(MediaType.parse("image/*"), farmerProfilePicture);
            if (requestFile != null) {
                uploadbody = MultipartBody.Part.createFormData("farmer_image", farmerProfilePicture.getName(), requestFile);
            }

        }

        RequestBody requestBodyFarmerName = RequestBody.create(MediaType.parse("text/plain"), fName);
        RequestBody requestBodyFarmerMobile = RequestBody.create(MediaType.parse("text/plain"), fMobile);
        RequestBody requestBodyFarmerLatitude = RequestBody.create(MediaType.parse("text/plain"), fLatitude);
        RequestBody requestBodyFarmerLongitude = RequestBody.create(MediaType.parse("text/plain"), fLongitude);
        RequestBody requestBodyFarmerState = RequestBody.create(MediaType.parse("text/plain"), farmerState);
        RequestBody requestBodyFarmerVillage = RequestBody.create(MediaType.parse("text/plain"), fVillage);
        RequestBody requestBodyFarmerLand = RequestBody.create(MediaType.parse("text/plain"), fLand);
        RequestBody requestBodyExecutiveId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(exeID));
        RequestBody requestBodySoilSample = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(isSoilCollected));

        Call<AddFarmerModel> call = apiService.addFarmerData(requestBodyFarmerName, requestBodyFarmerMobile, requestBodyFarmerLatitude, requestBodyFarmerLongitude, requestBodyFarmerVillage, requestBodyFarmerState, requestBodyFarmerLand, requestBodyExecutiveId, requestBodySoilSample, uploadbody);
        call.enqueue(new Callback<AddFarmerModel>() {
            @Override
            public void onResponse(Call<AddFarmerModel> call, Response<AddFarmerModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1) {
                    farmerId = String.valueOf(response.body().getData().get(0).getFarmer_id());
                    farmerUniqueId.setText(String.valueOf(response.body().getData().get(0).getFarmer_unique_id()));
                    Log.d(TAG, "Farmer Added successfully");
                    showToastAtCentre("Farmer Added Successfully.", LENGTH_SHORT);
                    outputFileUri = null;
                    farmerProfilePicture = null;
                    isFarmerLocallySaved = true;

                    saveFarmerDetailsBtn.setVisibility(View.INVISIBLE);
                    submitFarmerData.setVisibility(View.VISIBLE);
                    addCropText.setEnabled(true);
                    addCropText.setTextColor(Color.parseColor("#39B54A"));

                } else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<AddFarmerModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }

    public void updateFarmerDetails(String fName, String fMobile, String fLatitude, String fLongitude, String farmerState, String fVillage, String fLand, int isSoilCollected) {
        UtilsClass.showBusyAnimation(baseActivity, "Registering..");
        int exeID = SharedPreferenceUtility.getUserId(baseActivity);
        RequestBody requestFile = null;
        MultipartBody.Part uploadbody = null;

        if (farmerProfilePicture != null) {
            requestFile = RequestBody.create(MediaType.parse("image/*"), farmerProfilePicture);
            if (requestFile != null) {
                uploadbody = MultipartBody.Part.createFormData("farmer_image", farmerProfilePicture.getName(), requestFile);
            }

        }

        RequestBody requestBodyFarmerName = RequestBody.create(MediaType.parse("text/plain"), fName);
        RequestBody requestBodyFarmerMobile = RequestBody.create(MediaType.parse("text/plain"), fMobile);
        RequestBody requestBodyFarmerLatitude = RequestBody.create(MediaType.parse("text/plain"), fLatitude);
        RequestBody requestBodyFarmerLongitude = RequestBody.create(MediaType.parse("text/plain"), fLongitude);
        RequestBody requestBodyFarmerState = RequestBody.create(MediaType.parse("text/plain"), farmerState);
        RequestBody requestBodyFarmerVillage = RequestBody.create(MediaType.parse("text/plain"), fVillage);
        RequestBody requestBodyFarmerLand = RequestBody.create(MediaType.parse("text/plain"), fLand);
        RequestBody requestBodyFarmerId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(farmerId));
        RequestBody requestBodySoilSample = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(isSoilCollected));

        Call<GeneralModel> call = apiService.updateFarmerData(requestBodyFarmerName, requestBodyFarmerMobile, requestBodyFarmerLatitude, requestBodyFarmerLongitude, requestBodyFarmerVillage, requestBodyFarmerState, requestBodyFarmerLand, requestBodyFarmerId, requestBodySoilSample, uploadbody);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1) {
//                    farmerId = String.valueOf(response.body().getData());
                    Log.d(TAG, "Farmer Updated successfully");
                    showToastAtCentre("Farmer Updated Successfully.", LENGTH_SHORT);
                    outputFileUri = null;
                    farmerProfilePicture = null;
                        finish();
                    if (screenSource.equalsIgnoreCase("farmer_details")) {
                        Intent intent = new Intent("update_farmer_data");
                        sendBroadcast(intent);
                    }
//                    loadFarmerData();
//                    isFarmerLocallySaved = false;
//                    isEditClicked = false;
//                    isSourceAddFarmer = false;
//                    adapter.setDeleteVisible(false);
//                    adapter.notifyDataSetChanged();
//                    takeAPic.setVisibility(View.INVISIBLE);
//                    saveFarmerDetailsBtn.setVisibility(View.INVISIBLE);
//                    submitFarmerData.setVisibility(View.INVISIBLE);
//                    cancelFarmerDetails.setVisibility(View.GONE);
//                    addCropText.setVisibility(View.GONE);
//                    deleteBtn.setVisibility(isToday ? View.VISIBLE : View.INVISIBLE);
//                    editBtn.setVisibility(isToday ? View.VISIBLE : View.INVISIBLE);
//                    bottomLayout.setVisibility(View.GONE);
//                    checkBoxLayout.setVisibility(View.VISIBLE);
//                    getLocation.setEnabled(false);
//                    farmerName.setEnabled(false);
//                    farmerMobile.setEnabled(false);
//                    farmerLocation.setEnabled(false);
//                    farmerLandArea.setEnabled(false);
//                    farmerVillage.setEnabled(false);
//                    farmerStateSpinner.setEnabled(false);


                } else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }

    public void loadFarmerData() {
        UtilsClass.showBusyAnimation(baseActivity, "Registering..");

        Call<FarmerDetailsModel> call = apiService.getFarmerDetails(farmerId);

        call.enqueue(new Callback<FarmerDetailsModel>() {
            @Override
            public void onResponse(Call<FarmerDetailsModel> call, Response<FarmerDetailsModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1) {
                    String farmerID = response.body().getData().get(0).getFarmer_id();
                    farmerName.setText(response.body().getData().get(0).getFarmer_name());
                    farmerMobile.setText(response.body().getData().get(0).getFarmer_mobile());
                    isAllCropsAdded = response.body().getData().get(0).getIs_crop_added().equalsIgnoreCase("1");
                    boolean isChecked = !response.body().getData().get(0).getSoil_sample().equalsIgnoreCase("0");
                    haveCollectedCropCheckbox.setChecked(isChecked);
                    isSoilSampleCollected = isChecked;
                    String location = response.body().getData().get(0).getFarmer_latitude() + ", " + response.body().getData().get(0).getFarmer_longitude();
                    farmerLocation.setText(location);
                    farmerVillage.setText(response.body().getData().get(0).getVillage_name());
                    farmerLandArea.setText(response.body().getData().get(0).getLand_area());

                    String farmerImagePath = response.body().getData().get(0).getFarmer_image();

                    // Vinoth: Here "80" is the size of imageview
                    int size = dpToPixel(80);
                    if (!TextUtils.isEmpty(farmerImagePath)) {

                        Picasso.with(baseActivity)
                                .load(farmerImagePath)
                                .placeholder(R.drawable.default_image)
                                .error(R.drawable.default_image)
                                .resize(size, size)
                                .centerInside()
                                .onlyScaleDown()
                                .into(farmerImage);
                    } else {

                        Picasso.with(baseActivity).load(R.drawable.default_image)
                                .into(farmerImage);
                    }
                    // set Spinner data here
                    String crop = response.body().getData().get(0).getState().trim();
                    farmerState.setText(crop);
//                    int index = 0;
//                    for (String cropType : stateList) {
//                        if (cropType.equalsIgnoreCase(crop)) {
//                            farmerStateSpinner.setSelection(index);
//                            break;
//                        }
//                        index++;
//                    }
                    farmerUniqueId.setText(response.body().getData().get(0).getFarmer_unique_id());
                    getCropsList(farmerID);
                    // load crop data here

                } else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<FarmerDetailsModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);

            }
        });
    }

    public int dpToPixel(int dp)
    {
        final float scale = baseActivity.getResources().getDisplayMetrics().density;
        // convert the DP into pixel
        return (int)(dp * scale + 0.5f);

    }


    public void deleteFarmer(String farmerId) {
        UtilsClass.showBusyAnimation(baseActivity, "Registering..");

        Call<GeneralModel> call = apiService.deleteFarmer(farmerId);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1) {
                    showToastAtCentre("Farmer deleted successfully", LENGTH_LONG);
                    if (screenSource.equalsIgnoreCase("farmer_details")) {
                        Intent intent = new Intent("update_farmer_data");
                        sendBroadcast(intent);
                    }
                    isFarmerLocallySaved = false;
                    finish();

                } else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);

            }
        });
    }

    public void getCropsList(final String farmerId) {
        UtilsClass.showBusyAnimation(baseActivity, "Registering..");

        Call<CropsModel> call = apiService.getAddedCropList(farmerId);

        call.enqueue(new Callback<CropsModel>() {
            @Override
            public void onResponse(Call<CropsModel> call, Response<CropsModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1) {
                    cropsList = response.body().getData();
                    adapter = new CropsAdapter(baseActivity, cropsList, farmerId, isToday);
                    if (!isEditClicked) {
                        adapter.setDeleteVisible(false);
                        adapter.notifyDataSetChanged();
                    }
                    else {
                        adapter.setDeleteVisible(true);
                        adapter.notifyDataSetChanged();
                    }
                    cropsRecyclerView.setLayoutManager(new GridLayoutManager(baseActivity, 1));
                    cropsRecyclerView.setAdapter(adapter);

                } else // Handle failure cases here
                {
                    if (response.body().getData().size() == 0)
                    {
                        cropsList = response.body().getData();
                        adapter = new CropsAdapter(baseActivity, cropsList, farmerId, isToday);
                        cropsRecyclerView.setLayoutManager(new GridLayoutManager(baseActivity, 1));
                        cropsRecyclerView.setAdapter(adapter);
                    }
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
//                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<CropsModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);

            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }

    public void showImageUploadDialog() {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.imagechooser_new);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;

        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        ImageView btnCam = dialog.findViewById(R.id.btnCamera);
        ImageView btnGallery = dialog.findViewById(R.id.btnGallery);
        TextView cancelView = dialog.findViewById(R.id.cancelView);


        final boolean result = false;

        btnCam.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                boolean isPermission = checkPermission(baseActivity, true);
                userChoosenTask = "Take Photo";
                if (isPermission)
                    cameraIntent();
                dialog.dismiss();

            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isPermission = checkPermission(baseActivity, false);
                userChoosenTask = "Choose from Library";
                if (isPermission)
                    galleryIntent();
                dialog.dismiss();
            }
        });

        cancelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public boolean checkPermission(final Context context, boolean isCamera) {

        boolean isPermission = false;

        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {

            if (!isCamera) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED  || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                    return false;
                } else {
                    isPermission = true;
                }
            } else {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
                    return false;
                } else {
                    isPermission = checkPermission(baseActivity, false);

                }

            }


        } else {
            isPermission = true;
        }


        return isPermission;
    }

    private void findUnAskedPermissions(ArrayList wanted) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList result = new ArrayList();

            for (Object perm : wanted) {
                if (!hasPermission((String) perm)) {
                    result.add(perm);
                }
            }

            permissionsToRequest = result;

            if (permissionsToRequest.size() > 0) {
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            } else {
                gotAllPermissions = true;
                gps = new GPSTracker(AddFarmerActivity.this);
            }
        }

    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {

                    checkPermission(baseActivity, false);
                }
                break;

            case MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {

                    checkPermission(baseActivity, true);
                }
                break;
            case ALL_PERMISSIONS_RESULT:
                permissionsRejected.clear();
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0).toString())) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                String[] arr = (String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]);
                                                requestPermissions(arr, ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                        }
                    }

                } else {
                    gotAllPermissions = true;
                    gps = new GPSTracker(AddFarmerActivity.this);
                    if (screenSource.equalsIgnoreCase("add_farmer"))
                    {
                        if (gps.isGPSTurnedOn()) {
                            gps.getLocation();
                            final String lat = String.valueOf(gps.getLatitude());
                            final String longi = String.valueOf(gps.getLongitude());
                            farmerLocation.setText(lat + ", " + longi);
                            Geocoder gcd = new Geocoder(baseActivity, Locale.getDefault());
                            List<Address> addresses = null;
                            try {
                                addresses = gcd.getFromLocation(Double.valueOf(lat), Double.valueOf(longi), 1);
                                if (addresses.size() > 0) {
                                    Log.d(TAG, "State Name: " + addresses.get(0).getAdminArea());
                                    String state = addresses.get(0).getAdminArea().trim();
                                    farmerState.setText(state);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            gps.showSettingsAlert();
                        }
                    }
                }
                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(AddFarmerActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void cameraIntent() {

//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Images.Media.TITLE, "New Picture");
//        values.put(MediaStore.Images.Media.DESCRIPTION, "From the camera");
//        outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
//        startActivityForResult(cameraIntent, REQUEST_CAMERA);

        Intent m_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
        outputFileUri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", file);
        m_intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(m_intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, SELECT_FILE);
    }

    private String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {
//                    farmerImage.setImageURI(outputFileUri);
//                    farmerProfilePicture = new File(getRealPathFromURI(outputFileUri));

//                    File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
//
//                    //Uri of camera image
//                    Uri uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", file);
                      uploadPicAfterClick(outputFileUri);
                }

                break;
            case SELECT_FILE:
                if (resultCode == RESULT_OK) {
//                    farmerImage.setImageURI(imageReturnedIntent.getData());
//                    farmerProfilePicture = new File(getRealPathFromURI(imageReturnedIntent.getData()));
                    uploadPicAfterClick(imageReturnedIntent.getData());
                }
                break;
            case ADD_CROP_REQUEST:
                getCropsList(farmerId);
                break;
        }
    }

    private Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void uploadPicAfterClick(Uri fileUri)
    {
        Bitmap thumbnail = null;
        try {
            thumbnail = MediaStore.Images.Media.getBitmap(baseActivity.getContentResolver(), fileUri);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (thumbnail != null) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail, thumbnail.getWidth(), thumbnail.getHeight(), true);
            Uri tempUri = getImageUri(getApplicationContext(), scaledBitmap);
            farmerProfilePicture = new File(getRealPathFromURI(tempUri));

            int size = dpToPixel(80);

                Picasso.with(baseActivity)
                        .load(tempUri)
                        .placeholder(R.drawable.default_image)
                        .error(R.drawable.default_image)
                        .resize(size, size)
                        .centerInside()
                        .onlyScaleDown()
                        .into(farmerImage);
//
//            Picasso.with(this).load(tempUri)
//                    .into(farmerImage);

//            farmerImage.setImageBitmap(scaledBitmap);

            scaledBitmap.recycle();
            thumbnail.recycle();
        }

    }

    private void confirmSubmit(String titleTxt, String desc, String cancelBtnText, String okBtnText, final boolean isReSubmit) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert_checkbox);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = dialog.findViewById(R.id.alertTitle1);
        final CheckBox isSoilCollected = dialog.findViewById(R.id.cartItemCheckView);
        boolean isSoilCollect = isSoilCollected.isChecked();
        if (isReSubmit && !isSourceAddFarmer) {
            isSoilCollected.setVisibility(View.GONE);
            isSoilCollect = isSoilSampleCollected;
        }
        TextView textDesc = dialog.findViewById(R.id.alertText);
        textDesc.setText(desc);

        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        title.setText(titleTxt);
        okBtn.setText(okBtnText);
        cancelBtn.setText(cancelBtnText);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        final boolean finalIsSoilCollect = isSoilCollect;
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkFarmerDataAndSave(!isReSubmit, finalIsSoilCollect ? 1 : 0);
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void confirmSubmitWithSoilData(String titleTxt, String desc, String cancelBtnText, String okBtnText) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert_checkbox);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = dialog.findViewById(R.id.alertTitle1);
        final CheckBox isSoilCollected = dialog.findViewById(R.id.cartItemCheckView);

        TextView textDesc = dialog.findViewById(R.id.alertText);
        textDesc.setText(desc);

        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        title.setText(titleTxt);
        okBtn.setText(okBtnText);
        cancelBtn.setText(cancelBtnText);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkFarmerDataAndSave(false, isSoilCollected.isChecked() ? 1 : 0);
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void confirmSubmitWithOUTSoilData(String titleTxt, String desc, String cancelBtnText, String okBtnText) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = dialog.findViewById(R.id.alertTitle1);

        TextView textDesc = dialog.findViewById(R.id.alertText);
        textDesc.setText(desc);

        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        title.setText(titleTxt);
        okBtn.setText(okBtnText);
        cancelBtn.setText(cancelBtnText);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkFarmerDataAndSave(false, isSoilSampleCollected ? 1 : 0);
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void confirmReSubmit() {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = dialog.findViewById(R.id.alertTitle1);
        TextView desc = dialog.findViewById(R.id.alertText);

        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        title.setText("Submit Farmer Details");
        desc.setText("Do you want to submit before going back?");
        okBtn.setText("Yes");
        cancelBtn.setText("No");

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                finish();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isSoilSampleCollected)
                {
                    confirmSubmitWithOUTSoilData("Are you sure to resubmit?","Resubmit Farmer Information?", "Cancel","Submit");
                }
                else {
                    confirmSubmitWithSoilData("Submit Farmer Information","Have you collected the soil sample?", "Cancel","Submit");
                }
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void confirmDeleteFarmer(String titleTxt, String titleDesc, String cancelBtnText, String okBtnText) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = dialog.findViewById(R.id.alertTitle1);
        TextView desc = dialog.findViewById(R.id.alertText);
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        title.setText(titleTxt);
        desc.setText(titleDesc);
        okBtn.setText(okBtnText);
        cancelBtn.setText(cancelBtnText);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                deleteFarmer(farmerId);
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onCropDataUpdated() {
        getCropsList(farmerId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}

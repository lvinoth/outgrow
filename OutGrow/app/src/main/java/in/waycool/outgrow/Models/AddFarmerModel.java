package in.waycool.outgrow.Models;

import java.util.List;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class AddFarmerModel {


    private int status;

    private String message;

    private List<FarmerData> data;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<FarmerData> getData() {
        return data;
    }

    public void setData(List<FarmerData> data) {
        this.data = data;
    }


    public class FarmerData
    {
        private String farmer_id;
        private String farmer_unique_id;

        public String getFarmer_unique_id() {
            return farmer_unique_id;
        }

        public void setFarmer_unique_id(String farmer_unique_id) {
            this.farmer_unique_id = farmer_unique_id;
        }

        public String getFarmer_id() {
            return farmer_id;
        }

        public void setFarmer_id(String farmer_id) {
            this.farmer_id = farmer_id;
        }
    }

}

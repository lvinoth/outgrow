package in.waycool.outgrow;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mukesh.OtpView;

import java.io.IOException;

import in.waycool.outgrow.Models.GetOTPModel;
import in.waycool.outgrow.Models.LoginModel;
import in.waycool.outgrow.Retrofit.ApiClient;
import in.waycool.outgrow.Retrofit.ApiInterface;
import in.waycool.outgrow.Utils.SharedPreferenceUtility;
import in.waycool.outgrow.Utils.UtilsClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;

public class VerifyPasswordActivity extends AppCompatActivity {

    private TextView verifyBtn;
    private TextView resendOTP;
    private OtpView otpPassword;
    private Activity baseActivity;
    private static final String TAG = VerifyPasswordActivity.class.getSimpleName();
    private Toast mToast;
    private ApiInterface apiService;
    private String mobileNumber;
    private ImageView backBtn;
    private String callingActivityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_password);
        baseActivity = this;
        callingActivityName = getCallingActivity().getClassName();
        mobileNumber = getIntent().getStringExtra("mobileNo");
        Log.d(TAG,"MobileNumber: " + mobileNumber);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        otpPassword = findViewById(R.id.otpPassword);
        backBtn = findViewById(R.id.backBtn);
        verifyBtn = findViewById(R.id.verifyBtn);
        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int len = otpPassword.getText().toString().length();
                if (len != 6)
                {
                    showToastAtCentre("Please enter the OTP", LENGTH_LONG);
                    return;
                }
                authorizeLoginUsingOtp(mobileNumber, otpPassword.getText().toString());
            }
        });

        resendOTP = findViewById(R.id.resendOtp);
        resendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOtp();
            }
        });
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public void authorizeLoginUsingOtp(String mobileNo, String otp)
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        Call<LoginModel> call = apiService.executiveLogin(mobileNo, otp);

        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    Log.d(TAG, "Login Success!");
                    int userId = Integer.parseInt(response.body().getData().get(0).getField_executive_id());
                    String mobileNum = response.body().getData().get(0).getField_executive_mobile();
                    String name = response.body().getData().get(0).getField_executive_name();
                    String lat =  response.body().getData().get(0).getField_executive_latitude();
                    String longi = response.body().getData().get(0).getField_executive_longitude();
                    String image = response.body().getData().get(0).getField_executive_image();
                    SharedPreferenceUtility.saveUserLoginDetails(getApplicationContext(),userId, mobileNum, name, lat, longi, image);
                    Intent intent = getIntent();
                    setResult(RESULT_OK, intent);
                    startActivity(new Intent(baseActivity, DrawerActivity.class));
                    finish();
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }

    public void getOtp()
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        Call<GetOTPModel> call = apiService.getOTP(mobileNumber);

        call.enqueue(new Callback<GetOTPModel>() {
            @Override
            public void onResponse(Call<GetOTPModel> call, Response<GetOTPModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    showToastAtCentre("Otp has been sent to your mobile number", LENGTH_SHORT);
                    Log.d(TAG, "Otp sent successfully to your mobile number");
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<GetOTPModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }
}

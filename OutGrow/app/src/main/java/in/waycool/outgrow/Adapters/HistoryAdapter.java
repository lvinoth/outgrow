package in.waycool.outgrow.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.waycool.outgrow.AddFarmerActivity;
import in.waycool.outgrow.HistoryActivity;
import in.waycool.outgrow.Models.CropsModel;
import in.waycool.outgrow.Models.HistoryModel;
import in.waycool.outgrow.R;
import in.waycool.outgrow.Retrofit.ApiClient;
import in.waycool.outgrow.Retrofit.ApiInterface;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder>  {

    private List<HistoryModel.History> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private Activity baseActivity;
    private ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

    // data is passed into the constructor
    public HistoryAdapter(Context context, List<HistoryModel.History> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.baseActivity = (HistoryActivity)context;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.history_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final HistoryModel.History history = mData.get(position);
        holder.hisCount.setText(history.getCount());
        holder.hisDate.setText(history.getDate());


//        holder.recipeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent recipeDetail = new Intent(mContext.getApplicationContext(), RecipeDetailedActivity.class);
//                recipeDetail.putExtra("recipeID",farmer.getRecipeID().trim());
//                recipeDetail.putExtra("PARENT_PAGE", "REC");
//                if (farmer.getFlag().equals("3")){
//
//                    recipeDetail.putExtra("source","api");
//                    recipeDetail.putExtra("tab","0");
//                }
//                else {
//                    recipeDetail.putExtra("tab", selectedTab.trim());
//                }
//                mContext.startActivity(recipeDetail);
//            }
//        });

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView hisDate;
        TextView hisCount;


        ViewHolder(View itemView) {
            super(itemView);
            hisDate = itemView.findViewById(R.id.date);
            hisCount = itemView.findViewById(R.id.count);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
//
//    private void setDiffTypeFace(String convertedMoney, TextView view)    {
//        String convertedMoneyString = "₹ " + convertedMoney;
//        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/poppins_bold.otf");
//        SpannableStringBuilder SS = new SpannableStringBuilder(convertedMoneyString);
//        SS.setSpan (new CustomTypefaceSpan("", font), 0, 2, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
//        view.setText(SS);
//    }

    // convenience method for getting data at click position
    HistoryModel.History getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

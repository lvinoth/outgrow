package in.waycool.outgrow.Models;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class RegistrationModel {


    private int status;

    private String message;

    private int data;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}

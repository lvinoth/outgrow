package in.waycool.outgrow;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import in.waycool.outgrow.Utils.SharedPreferenceUtility;
import in.waycool.outgrow.Utils.UtilsClass;

public class SplashActivity extends AppCompatActivity {

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        setContentView(R.layout.activity_splash);
        showMainScreen();
    }

    private void showMainScreen() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Launch Main Activity here

                if (SharedPreferenceUtility.getUserId(getApplicationContext()) == -1) {
                    Intent i = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(getBaseContext(), DrawerActivity.class);
                    startActivity(i);
                }

                finish();
            }
        }, 2000);
    }
}

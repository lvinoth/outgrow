package in.waycool.outgrow.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import in.waycool.outgrow.R;

public class SharedPreferenceUtility {

    private static SharedPreferences sharedPreferences;


    public static SharedPreferences getSharedPreferenceInstance(Context context) {
        if (sharedPreferences == null)
            sharedPreferences = context.getSharedPreferences(
                    context.getString(R.string.USER_PREFERENCES),
                    Context.MODE_PRIVATE);

        return sharedPreferences;
    }

    //* SAVING USER ID & TOKEN *//
    public static void saveUserLoginDetails(Context context, int userID, String phone, String name, String lat, String longi, String image) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();

        sharedPreferencesEditor.putInt("executive_id", userID);
        sharedPreferencesEditor.putString("executive_mobile", phone);
        sharedPreferencesEditor.putString("executive_name", name);
        sharedPreferencesEditor.putString("executive_latitude", lat);
        sharedPreferencesEditor.putString("executive_longitude", longi);
        sharedPreferencesEditor.putString("executive_image", image);
        sharedPreferencesEditor.commit();
    }

    public static void saveExecutiveImage(Context context, String uri)
    {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();

        sharedPreferencesEditor.putString("executive_image", uri);
        sharedPreferencesEditor.commit();
    }



    public static void savePreferencesForLogout(Context context) {

        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();
        sharedPreferencesEditor.remove("executive_id");
        sharedPreferencesEditor.remove("executive_mobile");
        sharedPreferencesEditor.remove("executive_name");
        sharedPreferencesEditor.remove("executive_latitude");
        sharedPreferencesEditor.remove("executive_longitude");
        sharedPreferencesEditor.remove("executive_image");
        sharedPreferencesEditor.commit();
    }


    public static int getUserId(Context context) {
        return getSharedPreferenceInstance(context).getInt("executive_id", -1);
    }

    public static String getUserName(Context context) {
        return getSharedPreferenceInstance(context).getString("executive_name", "");
    }

    public static String getUserMobile(Context context) {
        return getSharedPreferenceInstance(context).getString("executive_mobile", "");
    }

    public static String getUserImage(Context context) {
        return getSharedPreferenceInstance(context).getString("executive_image", "");
    }
}

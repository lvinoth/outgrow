package in.waycool.outgrow;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.dewinjm.monthyearpicker.MonthYearPickerDialog;
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import in.waycool.outgrow.Adapters.CropsAdapter;
import in.waycool.outgrow.Adapters.HistoryAdapter;
import in.waycool.outgrow.Models.CropsModel;
import in.waycool.outgrow.Models.HistoryModel;
import in.waycool.outgrow.Retrofit.ApiClient;
import in.waycool.outgrow.Retrofit.ApiInterface;
import in.waycool.outgrow.Utils.SharedPreferenceUtility;
import in.waycool.outgrow.Utils.UtilsClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;

public class HistoryActivity extends AppCompatActivity {

    private static final String TAG = HistoryActivity.class.getSimpleName();
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;
    private ImageView goBack;
    private TextView totalCount;
    private TabLayout monthTab;
    private TextView noDataView;

    private RecyclerView historyRecyclerView;
    private List<HistoryModel.History> historyList;
    private HistoryAdapter adapter;
    private DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        baseActivity = this;
        monthTab = findViewById(R.id.tabLayout);
        monthTab.addOnTabSelectedListener(tabSelectedListener);
        goBack = findViewById(R.id.backBtn);
        totalCount = findViewById(R.id.totalCount);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        noDataView = findViewById(R.id.noDataView);
        historyRecyclerView = findViewById(R.id.historyRecyclerView);
        historyList= new ArrayList<>();
        adapter = new HistoryAdapter(baseActivity, historyList);
        monthTab.getTabAt(1).select();
    }

    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            String param = "cur_month";
            String fromDate = "";
            String toDate = "";

            switch (tab.getPosition())
            {
                case 0:
                    getDates();
                    noDataView.setText("No farmer registered");
                    param="search";
                    return;
                case 1:
                    noDataView.setText("No farmer registered this month");
                    param="cur_month";
                    break;
                case 2:
                    noDataView.setText("No farmer registered last month");
                    param="last_month";
                    break;
            }
            getMyHistory(param, fromDate, toDate);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    private void getDates() {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert_calendar);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        final TextInputEditText title = dialog.findViewById(R.id.startDate);
        title.setHint("Start Month");
        title.setKeyListener(null);
//        title.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDatePicker(title, true);
//            }
//        });
        title.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    showDatePicker(title, true);
                }
            }
        });
        final TextInputEditText desc = dialog.findViewById(R.id.endDate);
        desc.setHint("End Month");
        desc.setKeyListener(null);
//        desc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDatePicker(desc, false);
//            }
//        });

        desc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    showDatePicker(desc, false);
                }
            }
        });

        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monthTab.getTabAt(1).select();
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                if (TextUtils.isEmpty(title.getText().toString().trim()))
                {
                    showToastAtCentre("Enter start month",LENGTH_SHORT);
                    return;
                }
                if (TextUtils.isEmpty(desc.getText().toString().trim()))
                {
                    showToastAtCentre("Enter end month",LENGTH_SHORT);
                    return;
                }
                if (selectedStartDate.after(selectedEndDate))
                {
                    showToastAtCentre("Start month should not be after end month", LENGTH_SHORT);
                    return;
                }
                if (selectedStartDate.get(Calendar.MONTH)==selectedEndDate.get(Calendar.MONTH) && (selectedStartDate.get(Calendar.YEAR)==selectedEndDate.get(Calendar.YEAR)))
                {
                    showToastAtCentre("Start month and end month should not be the same", LENGTH_SHORT);
                    return;
                }
                getMyHistory("search", title.getText().toString().trim(), desc.getText().toString().trim());
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private Calendar selectedStartDate;
    private Calendar selectedEndDate;
    public void showDatePicker(final TextInputEditText date, final boolean isStart)
    {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

        int yearSelected;
        int monthSelected;

        Calendar calendar = Calendar.getInstance();
        yearSelected = calendar.get(Calendar.YEAR);
        c.set(mYear, mMonth, mDay);
        long maxDate = c.getTimeInMillis();

        monthSelected = calendar.get(Calendar.MONTH);

        MonthYearPickerDialogFragment dialogFragment = MonthYearPickerDialogFragment
                .getInstance(monthSelected, yearSelected,0,maxDate);

        dialogFragment.setOnDateSetListener(new MonthYearPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int year, int monthOfYear) {
                // do something
                Log.d(TAG,"Month: " + monthOfYear);
                Log.d(TAG,"Year: " + year);

                date.setText(String.valueOf(monthOfYear + 1));

                Calendar isDateSelected = Calendar.getInstance();
                isDateSelected.set(year,monthOfYear, 1);
                if (isStart)
                {
                    selectedStartDate = isDateSelected;
                }else
                {
                    selectedEndDate = isDateSelected;
                }
            }
        });
        dialogFragment.show(getSupportFragmentManager(), null);
    }

    public void getMyHistory(String requestType, String fromDate, String toDate)
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");
        int exeID = SharedPreferenceUtility.getUserId(baseActivity);
        Call<HistoryModel> call = apiService.getHistoryList(String.valueOf(exeID),requestType,fromDate,toDate);

        call.enqueue(new Callback<HistoryModel>() {
            @Override
            public void onResponse(Call<HistoryModel> call, Response<HistoryModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    historyList = response.body().getData();
                    if (historyList.size() > 0){
                        String preppend = "Total Submitted Reports - "  + historyList.get(0).getTot_count();
                        totalCount.setText(preppend);
                        historyRecyclerView.setVisibility(View.VISIBLE);
                        noDataView.setVisibility(View.GONE);
                    }
                    else {
                        String preppend = "Total Submitted Reports - 0";
                        totalCount.setText(preppend);
                        historyRecyclerView.setVisibility(View.GONE);
                        noDataView.setVisibility(View.VISIBLE);
                    }
                    adapter = new HistoryAdapter(baseActivity, historyList);
                    historyRecyclerView.setLayoutManager(new GridLayoutManager(baseActivity, 1));
                    historyRecyclerView.setAdapter(adapter);
                }
                else // Handle failure cases here
                {
                    String preppend = "Total Submitted Reports - 0";
                    totalCount.setText(preppend);
                    historyList = response.body().getData();
                    historyRecyclerView.setVisibility(View.GONE);
                    noDataView.setVisibility(View.VISIBLE);
                    adapter = new HistoryAdapter(baseActivity, historyList);
                    historyRecyclerView.setLayoutManager(new GridLayoutManager(baseActivity, 1));
                    historyRecyclerView.setAdapter(adapter);
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    //showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<HistoryModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);

            }
        });
    }


    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }
}

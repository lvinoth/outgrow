package in.waycool.outgrow;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import in.waycool.outgrow.Adapters.FarmersAdapter;
import in.waycool.outgrow.Models.FarmersModel;
import in.waycool.outgrow.Models.GeneralModel;
import in.waycool.outgrow.Retrofit.ApiClient;
import in.waycool.outgrow.Retrofit.ApiInterface;
import in.waycool.outgrow.Utils.SharedPreferenceUtility;
import in.waycool.outgrow.Utils.UtilsClass;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FarmersAdapter.FarmerUpdateInterface {

    private static final int ADD_FARMER_REQUEST = 1001;
    private ImageView addFarmer;
    private Activity baseActivity;
    private static final String TAG = DrawerActivity.class.getSimpleName();
    private Toast mToast;
    private ApiInterface apiService;

    private RecyclerView farmersRecyclerView;
    private List<FarmersModel.Farmers> farmersList;
    private FarmersAdapter adapter;
    private TabLayout dateTab;
    private String fromDate;
    private String toDate;
    private String requestType = "today";
    private TextView noOfReports;
    private TextView daysFarmerInformation;
    private TextView noDataView;
    DatePickerDialog datePickerDialog;
    private String userChoosenTask = "";
    private static final int REQUEST_CAMERA = 101;
    private static final int SELECT_FILE = 102;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 456;
    private Uri outputFileUri;
    private File farmerProfilePicture;
    private ImageView executiveProfilePic;
    private ImageView takePhoto;
    private TextView logoutBtn;
    private boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        baseActivity = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        farmersRecyclerView = findViewById(R.id.farmerInformationRecyclerView);
        farmersList = new ArrayList<>();
        adapter = new FarmersAdapter(baseActivity, farmersList, false);
        noOfReports = findViewById(R.id.noOfReports);
        daysFarmerInformation = findViewById(R.id.daysFarmerInformation);
        noDataView = findViewById(R.id.noDataView);
        dateTab = findViewById(R.id.dayTabLayout);
        dateTab.addOnTabSelectedListener(tabSelectedListener);

        addFarmer = findViewById(R.id.addFarmer);
        addFarmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addFarmer = new Intent(baseActivity, AddFarmerActivity.class);
                addFarmer.putExtra("farmer_id","");
                addFarmer.putExtra("source", "add_farmer");
                addFarmer.putExtra("isToday", true);
                addFarmer.putExtra("crops_added", false);
                startActivityForResult(addFarmer, ADD_FARMER_REQUEST);
            }
        });
        registerReceiver(broadcastReceiver, new IntentFilter("update_farmer_data"));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        View navHeaderView =  navigationView.getHeaderView(0);
        executiveProfilePic = navHeaderView.findViewById(R.id.executiveImageView);
        executiveProfilePic.setClipToOutline(true);

        String uriString = SharedPreferenceUtility.getUserImage(baseActivity);
        if (!TextUtils.isEmpty(uriString)){

            if(uriString.contains("http:"))
            {
                int size = dpToPixel(60);

                Picasso.with(baseActivity)
                        .load(uriString)
                        .placeholder(R.drawable.default_image)
                        .error(R.drawable.default_image)
                        .resize(size, size)
                        .centerInside()
                        .onlyScaleDown()
                        .into(executiveProfilePic);
//
//                Picasso.with(baseActivity).load(uriString)
//                    .placeholder(R.drawable.outgrow_logo_final_01)
//                    .error(R.drawable.outgrow_logo_final_01)
//                    .into(executiveProfilePic);
            }
        }

        takePhoto = navHeaderView.findViewById(R.id.takeAPic);
        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageUploadDialog();
            }
        });
        logoutBtn = findViewById(R.id.logout);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmLogout("Logout", "Do you want to logout?", "No", "Yes");
            }
        });
        TextView navUsername = (TextView) navHeaderView.findViewById(R.id.executiveNameDrawer);
        TextView navUserMobile = (TextView) navHeaderView.findViewById(R.id.executiveMobileDrawer);
        navUsername.setText(SharedPreferenceUtility.getUserName(this));
        navUserMobile.setText("+91 " + SharedPreferenceUtility.getUserMobile(this));
        dateTab.getTabAt(1).select();
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action.equals("update_farmer_data")) {
                getFarmersList(requestType, fromDate, toDate);
            }
        }
    };

    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            requestType = "today";
//            String fromDate = "";
//            String toDate = "";

            switch (tab.getPosition())
            {
                case 0:
                    getDates();
                    daysFarmerInformation.setText("Farmer Information");
                    noDataView.setText("No farmer registered");
                    requestType="search";
                    return;
                case 1:
                    daysFarmerInformation.setText("Today's Farmer Information");
                    noDataView.setText("No farmer registered today");
                    requestType="today";
                    break;
                case 2:
                    daysFarmerInformation.setText("Yesterday's Farmer Information");
                    noDataView.setText("No farmer registered yesterday");
                    requestType="yesterday";
                    break;
            }
            getFarmersList(requestType, fromDate, toDate);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    private void getDates() {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert_calendar);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        final TextInputEditText title = dialog.findViewById(R.id.startDate);
        title.setHint("Start Date");
        title.setKeyListener(null);
//        title.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDatePicker(title, true);
//            }
//        });
        title.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    showDatePicker(title, true);
                }
            }
        });
        final TextInputEditText desc = dialog.findViewById(R.id.endDate);
        desc.setHint("End Date");
        desc.setKeyListener(null);
//        desc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDatePicker(desc, false);
//            }
//        });
        desc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    showDatePicker(desc, false);
                }
            }
        });
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTab.getTabAt(1).select();
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                if (TextUtils.isEmpty(title.getText().toString().trim()))
                {
                    showToastAtCentre("Enter start date",LENGTH_SHORT);
                    return;
                }
                if (TextUtils.isEmpty(desc.getText().toString().trim()))
                {
                    showToastAtCentre("Enter end date",LENGTH_SHORT);
                    return;
                }
                if (selectedStartDate.after(selectedEndDate))
                {
                    showToastAtCentre("Start date should not be after end date", LENGTH_SHORT);
                    return;
                }
                if (selectedStartDate.get(Calendar.MONTH)==selectedEndDate.get(Calendar.MONTH) && (selectedStartDate.get(Calendar.YEAR)==selectedEndDate.get(Calendar.YEAR)) && (selectedStartDate.get(Calendar.DAY_OF_MONTH)==selectedEndDate.get(Calendar.DAY_OF_MONTH)))
                {
                    showToastAtCentre("Start date and end date should not be the same", LENGTH_SHORT);
                    return;
                }
                getFarmersList("search", title.getText().toString().trim(), desc.getText().toString().trim());
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private Calendar selectedStartDate;
    private Calendar selectedEndDate;
    public void showDatePicker(final TextInputEditText date, final boolean isStart)
    {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(DrawerActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            Calendar isDateSelected = Calendar.getInstance();
                            isDateSelected.set(year, monthOfYear, dayOfMonth);
                            String dateString = sdf.format(isDateSelected.getTime());
                        date.setText(dateString);

                        if (isStart)
                        {
                            selectedStartDate = isDateSelected;
                        }else
                            {
                                selectedEndDate = isDateSelected;
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void confirmLogout(String titleTxt, String titleDesc, String cancelBtnText, String okBtnText) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = dialog.findViewById(R.id.alertTitle1);
        TextView desc = dialog.findViewById(R.id.alertText);
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn = dialog.findViewById(R.id.alertOk);

        title.setText(titleTxt);
        desc.setText(titleDesc);
        okBtn.setText(okBtnText);
        cancelBtn.setText(cancelBtnText);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                SharedPreferenceUtility.savePreferencesForLogout(baseActivity);
                startActivity(new Intent(baseActivity, MainActivity.class));
                finish();
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    public void showImageUploadDialog() {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.imagechooser_new);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;

        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        ImageView btnCam = dialog.findViewById(R.id.btnCamera);
        ImageView btnGallery = dialog.findViewById(R.id.btnGallery);
        TextView cancelView = dialog.findViewById(R.id.cancelView);


        final boolean result = false;

        btnCam.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                boolean isPermission = checkPermission(baseActivity, true);
                userChoosenTask = "Take Photo";
                if (isPermission)
                    cameraIntent();
                dialog.dismiss();

            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isPermission = checkPermission(baseActivity, false);
                userChoosenTask = "Choose from Library";
                if (isPermission)
                    galleryIntent();
                dialog.dismiss();
            }
        });

        cancelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void uploadExecutiveImage(final String imageUri) {
        UtilsClass.showBusyAnimation(baseActivity, "Registering..");
        int exeID = SharedPreferenceUtility.getUserId(baseActivity);
        RequestBody requestFile = null;
        MultipartBody.Part uploadbody = null;

        if (farmerProfilePicture != null) {
            requestFile = RequestBody.create(MediaType.parse("image/*"), farmerProfilePicture);
            if (requestFile != null) {
                uploadbody = MultipartBody.Part.createFormData("fe_image", farmerProfilePicture.getName(), requestFile);
            }

        }

        RequestBody requestBodyFarmerId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(exeID));

        Call<GeneralModel> call = apiService.uploadExecutiveImage(requestBodyFarmerId, uploadbody);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1) {
//                    farmerId = String.valueOf(response.body().getData());
                    SharedPreferenceUtility.saveExecutiveImage(baseActivity, response.body().getData().trim());
                    Log.d(TAG, "Image Uploaded successfully");
                    showToastAtCentre("Image Uploaded successfully.", LENGTH_SHORT);
                    outputFileUri = null;
                    farmerProfilePicture = null;


                } else // Handle failure cases here
                {
//                    SharedPreferenceUtility.saveExecutiveImage(baseActivity, imageUri);
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }
    public boolean checkPermission(final Context context, boolean isCamera) {

        boolean isPermission = false;

        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {

            if (!isCamera) {
                if (ContextCompat.checkSelfPermission(context, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    return false;
                } else {
                    isPermission = true;
                }
            } else {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
                    return false;
                } else {
                    isPermission = checkPermission(baseActivity, false);

                }

            }


        } else {
            isPermission = true;
        }


        return isPermission;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {

                    checkPermission(baseActivity, false);
                }
                break;

            case MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {

                    checkPermission(baseActivity, true);
                }
                break;
        }

    }

    private void cameraIntent() {

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the camera");
        outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    private void galleryIntent() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, SELECT_FILE);
    }

    private String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                if (mToast != null) {
                    mToast.cancel();
                }
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            mToast = Toast.makeText(this, "Tap again to exit", Toast.LENGTH_SHORT);
            mToast.show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_referAFriend) {
            startActivity(new Intent(baseActivity, ReferActivity.class));

        } else if (id == R.id.nav_history) {
            startActivity(new Intent(baseActivity, HistoryActivity.class));
        }
        else if(id == R.id.nav_soil_sample)
        {
            Intent intent = new Intent(baseActivity, CampaignActivity.class);
            intent.putExtra("is_campaign", false);
            startActivity(intent);
        }
        else if(id == R.id.nav_campaign)
        {
            Intent intent = new Intent(baseActivity, CampaignActivity.class);
            intent.putExtra("is_campaign", true);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getFarmersList(String type, String fromDate, String toDate)
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        int exeID = SharedPreferenceUtility.getUserId(baseActivity);
        Log.d(TAG, "Executive Id:" + exeID);
        Call<FarmersModel> call = apiService.getFarmersList(String.valueOf(exeID), type,fromDate,toDate);

        call.enqueue(new Callback<FarmersModel>() {
            @Override
            public void onResponse(Call<FarmersModel> call, Response<FarmersModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                addFarmer.setVisibility(requestType.equalsIgnoreCase("today")? View.VISIBLE :View.INVISIBLE);
                if (response.body().getStatus() == 1)
                {
                    Log.d(TAG, "Login Success!");
                    farmersList = response.body().getData();
                    noOfReports.setText(farmersList.size() + " reports");
                    if (farmersList.size() > 0)
                    {
                        noDataView.setVisibility(View.INVISIBLE);
                        farmersRecyclerView.setVisibility(View.VISIBLE);
                        adapter = new FarmersAdapter(baseActivity, farmersList, requestType.equalsIgnoreCase("today"));
                        adapter.notifyDataSetChanged();
                        farmersRecyclerView.setLayoutManager(new GridLayoutManager(baseActivity, 1));
                        farmersRecyclerView.setAdapter(adapter);
                    }
                    else {
                        noDataView.setVisibility(View.VISIBLE);
                        farmersRecyclerView.setVisibility(View.INVISIBLE);
                        adapter = new FarmersAdapter(baseActivity, farmersList, requestType.equalsIgnoreCase("today"));
                        adapter.notifyDataSetChanged();
                        farmersRecyclerView.setLayoutManager(new GridLayoutManager(baseActivity, 1));
                        farmersRecyclerView.setAdapter(adapter);
                    }
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    farmersList = response.body().getData();
                    noOfReports.setText(farmersList.size() + " reports");
                    if (farmersList.size() > 0)
                    {
                        noDataView.setVisibility(View.INVISIBLE);
                        farmersRecyclerView.setVisibility(View.VISIBLE);
                        adapter = new FarmersAdapter(baseActivity, farmersList, requestType.equalsIgnoreCase("today"));
                        adapter.notifyDataSetChanged();
                        farmersRecyclerView.setLayoutManager(new GridLayoutManager(baseActivity, 1));
                        farmersRecyclerView.setAdapter(adapter);
                    }
                    else {
                        noDataView.setVisibility(View.VISIBLE);
                        farmersRecyclerView.setVisibility(View.INVISIBLE);
                        adapter = new FarmersAdapter(baseActivity, farmersList, requestType.equalsIgnoreCase("today"));
                        adapter.notifyDataSetChanged();
                        farmersRecyclerView.setLayoutManager(new GridLayoutManager(baseActivity, 1));
                        farmersRecyclerView.setAdapter(adapter);
                    }
                    Log.d(TAG, "Response: " + mess);
                    //showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<FarmersModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }

    private Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode)
        {
            case ADD_FARMER_REQUEST:
                getFarmersList(requestType, fromDate, toDate);
                break;
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {

//                    executiveProfilePic.setImageURI(outputFileUri);
//                    farmerProfilePicture = new File(getRealPathFromURI(outputFileUri));
                    uploadPicAfterClick(outputFileUri);

                }

                break;
            case SELECT_FILE:
                if (resultCode == RESULT_OK) {
//                    executiveProfilePic.setImageURI(data.getData());
//                    farmerProfilePicture = new File(getRealPathFromURI(data.getData()));
//                    uploadExecutiveImage(data.getData().toString());
                    uploadPicAfterClick(data.getData());
                }
                break;
        }
    }

    public int dpToPixel(int dp)
    {
        final float scale = baseActivity.getResources().getDisplayMetrics().density;
        // convert the DP into pixel
        return (int)(dp * scale + 0.5f);

    }

    public void uploadPicAfterClick(Uri fileUri)
    {
        Bitmap thumbnail = null;
        try {
            thumbnail = MediaStore.Images.Media.getBitmap(baseActivity.getContentResolver(), fileUri);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (thumbnail != null) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail, thumbnail.getWidth(), thumbnail.getHeight(), true);
            Uri tempUri = getImageUri(getApplicationContext(), scaledBitmap);
            farmerProfilePicture = new File(getRealPathFromURI(tempUri));

            int size = dpToPixel(60);

            Picasso.with(baseActivity)
                    .load(tempUri)
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .resize(size, size)
                    .centerInside()
                    .onlyScaleDown()
                    .into(executiveProfilePic);

//            Picasso.with(this).load(farmerProfilePicture)
//                    .placeholder(R.drawable.outgrow_logo_final_01)
//                    .error(R.drawable.outgrow_logo_final_01).into(executiveProfilePic);

        }
        uploadExecutiveImage(fileUri.toString());
    }


    @Override
    public void onFarmerDataUpdated() {
        getFarmersList(requestType, fromDate, toDate);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}

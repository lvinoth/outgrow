package in.waycool.outgrow.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import in.waycool.outgrow.AddCropActivity;
import in.waycool.outgrow.AddFarmerActivity;
import in.waycool.outgrow.Models.CropsModel;
import in.waycool.outgrow.Models.GeneralModel;
import in.waycool.outgrow.R;
import in.waycool.outgrow.Retrofit.ApiClient;
import in.waycool.outgrow.Retrofit.ApiInterface;
import in.waycool.outgrow.Utils.UtilsClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class CropsAdapter extends RecyclerView.Adapter<CropsAdapter.ViewHolder>  {

    private static final String TAG = CropsAdapter.class.getSimpleName();
    private List<CropsModel.CropData> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private Activity baseActivity;
    private ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private CropUpdateInterface cropUpdateInterface;
    private Toast mToast;
    private String farmerId;
//    private boolean isEnabled;
    private boolean isToday;
    private boolean isDeleteVisible;

    public void setDeleteVisible(boolean deleteVisible) {
        isDeleteVisible = deleteVisible;
    }

//    public void setIsEnBled(boolean enable)
//    {
//        this.isEnabled = enable;
//    }

    public interface CropUpdateInterface {
        void onCropDataUpdated();
    }

    // data is passed into the constructor
    public CropsAdapter(Context context, List<CropsModel.CropData> data, String farmerId, boolean isToday) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.farmerId = farmerId;
        this.baseActivity = (AddFarmerActivity)context;
        this.cropUpdateInterface = (CropUpdateInterface)context;
        this.isToday = isToday;
//        this.isEnabled = isToday;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public CropsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.crop_details_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CropsModel.CropData crop = mData.get(position);
        holder.cropName.setText(crop.getCrop_name());
        holder.cropDuration.setText(crop.getStart_date() +" - "  + crop.getEnd_date());
        holder.totalCost.setText(crop.getTotal_cost());
        holder.totalSale.setText(crop.getTotal_sale_amount());

        holder.deleteCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmDeleteCrop("Delete Crop Information", "Are you sure to delete?","Cancel", "Delete", crop.getCrop_details_id());
            }
        });

        holder.cropLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (!isEnabled)
//                {
//                    return;
//                }
//                Intent farmerDetail = new Intent(mContext.getApplicationContext(), AddCropActivity.class);
//                farmerDetail.putExtra("farmer_id",farmerId);
//                farmerDetail.putExtra("crop_id",crop.getCrop_details_id().trim());
//                farmerDetail.putExtra("source", "crop_details");
//                mContext.startActivity(farmerDetail);
            }
        });

//        holder.deleteCrop.setVisibility((isToday && isEnabled)? View.VISIBLE: View.INVISIBLE);
        holder.deleteCrop.setVisibility((isToday && isDeleteVisible)? View.VISIBLE: View.INVISIBLE);

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView cropName;
        TextView cropDuration;
        TextView totalCost;
        TextView totalSale;
        ImageView deleteCrop;
        RelativeLayout cropLayout;


        ViewHolder(View itemView) {
            super(itemView);
            cropName = itemView.findViewById(R.id.cropName);
            cropDuration = itemView.findViewById(R.id.cropDuration);
            totalCost = itemView.findViewById(R.id.totalCostValue);
            totalSale = itemView.findViewById(R.id.totalSaleValue);
            deleteCrop = itemView.findViewById(R.id.dialogClose);
            cropLayout = itemView.findViewById(R.id.childLayout);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
//
//    private void setDiffTypeFace(String convertedMoney, TextView view)    {
//        String convertedMoneyString = "₹ " + convertedMoney;
//        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/poppins_bold.otf");
//        SpannableStringBuilder SS = new SpannableStringBuilder(convertedMoneyString);
//        SS.setSpan (new CustomTypefaceSpan("", font), 0, 2, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
//        view.setText(SS);
//    }

    // convenience method for getting data at click position
    CropsModel.CropData getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void deleteCrop(String cropId)
    {
        UtilsClass.showBusyAnimation(baseActivity,"Registering..");

        Call<GeneralModel> call = apiService.deleteCrop(cropId);

        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {

                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus() == 1)
                {
                    showToastAtCentre("Crop deleted successfully", LENGTH_LONG);
                    cropUpdateInterface.onCropDataUpdated();
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("" + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
//                showToastAtCentre("" + t.getMessage(), LENGTH_LONG);
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(mContext, message, duration);
        mToast.show();
    }

    private void confirmDeleteCrop(String titleTxt, String titleDesc, String cancelBtnText , String okBtnText, final String cropID) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_alert);

        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
        dialog.getWindow().setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = dialog.findViewById(R.id.alertTitle1);
        TextView desc = dialog.findViewById(R.id.alertText);
        TextView cancelBtn = dialog.findViewById(R.id.alertCancel);
        TextView okBtn  = dialog.findViewById(R.id.alertOk);

        title.setText(titleTxt);
        desc.setText(titleDesc);
        okBtn.setText(okBtnText);
        cancelBtn.setText(cancelBtnText);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Invoke Delete API here
                deleteCrop(cropID);
                dialog.cancel();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }
}

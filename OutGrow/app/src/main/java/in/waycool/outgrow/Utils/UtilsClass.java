package in.waycool.outgrow.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.NestedScrollView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.waycool.outgrow.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class UtilsClass {

    private static Dialog progressDialog;

    public static void makeStatusBarTransparent(Window window, Context context)
    {
        /*if (Build.VERSION.SDK_INT >= 23) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        else
        {
            window.setStatusBarColor(context.getResources().getColor(R.color.darker_gray));
        }*/
        ((Activity)context).requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    public static void showBusyAnimation(final Activity baseActivity, final String progressText) {

        baseActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                //Log.d(UtilsClass.class.getSimpleName(),"showBusyAnimation");

                if (progressDialog == null){
                    createLoader(baseActivity);
                    if(!progressDialog.isShowing()) {
                        progressDialog.show();
                    }
                }
                else {
                    if(progressDialog != null && !progressDialog.isShowing()) {
                        progressDialog.show();
                    }
                }

               /* LayoutInflater inflater = (LayoutInflater) baseActivity.getSystemService(LAYOUT_INFLATER_SERVICE);
                View progressLayout = inflater.inflate(R.layout.progressdialog, null);

                TextView loadingText = progressLayout.findViewById(R.id.tv_loadingmsg);

                loadingText.setText(progressText);

                progressDialog = new Dialog(baseActivity);
                progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setContentView(progressLayout);
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();*/
            }
        });
    }

    public static void createLoader(Context context)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View progressLayout = inflater.inflate(R.layout.progressdialog, null);

        TextView loadingText = progressLayout.findViewById(R.id.tv_loadingmsg);

        loadingText.setText("Loading..");

        progressDialog = new Dialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(progressLayout);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
    }



    public static void hideBusyAnimation(final Activity baseActivity) {

        baseActivity.runOnUiThread(new Runnable() {
            public void run() {
                //Log.d(UtilsClass.class.getSimpleName(),"hideBusyAnimation");

                try {
                    if ((progressDialog != null) && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                } finally {
                    progressDialog = null;
                }
            }
        });

    }
}
